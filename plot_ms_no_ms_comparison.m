function plot_ms_no_ms_comparison(msPerf, msStd, msDprime, msCi, msCounts, noMsPerf, noMsStd, noMsDprime, noMsCi, noMsCounts)

figure('position', [500, 200, 1100, 700])
subplot(1,2,1)
plot_trial_performance(noMsPerf, noMsStd, 0, '')
hNoMs = findobj(gcf, 'Color','k');
hNoMs.Color = rgb('DarkCyan');
subplot(1,2,2)
plot_trial_dprime(noMsDprime, noMsCi, 0, '')
hNoMs = findobj(gcf, 'Color','k');
hNoMs.Color = rgb('DarkCyan');
hold on
subplot(1,2,1)
plot_trial_performance(msPerf, msStd, 0, '')
hMs = findobj(gcf, 'Color','k');
subplot(1,2,2)
plot_trial_dprime(msDprime, msCi, 0, '')
text(-3, 3.7, sprintf('n = %i', msCounts(1)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(-3, 3.5, sprintf('n = %i', noMsCounts(1)), 'FontWeight', 'bold' ,'FontSize', 12)
text(13, 3.7, sprintf('n = %i', msCounts(2)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(13, 3.5, sprintf('n = %i', noMsCounts(2)), 'FontWeight', 'bold' ,'FontSize', 12)
text(26, 3.7, sprintf('n = %i', msCounts(3)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(26, 3.5, sprintf('n = %i', noMsCounts(3)), 'FontWeight', 'bold' ,'FontSize', 12)
text(35, 3.7, sprintf('n = %i', msCounts(4)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(35, 3.5, sprintf('n = %i', noMsCounts(4)), 'FontWeight', 'bold' ,'FontSize', 12)
text(38, 3.0, sprintf('n = %i', msCounts(5)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(38, 3.2, sprintf('n = %i', noMsCounts(5)), 'FontWeight', 'bold' ,'FontSize', 12)
legend([hMs hNoMs], {'Ms trials', 'No Ms trials'}, 'Location','SouthWest')
subplot(1,2,1)
text(-3, .93, sprintf('n = %i', msCounts(1)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(-3, .95, sprintf('n = %i', noMsCounts(1)), 'FontWeight', 'bold' ,'FontSize', 12)
text(13, .93, sprintf('n = %i', msCounts(2)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(13, .95, sprintf('n = %i', noMsCounts(2)), 'FontWeight', 'bold' ,'FontSize', 12)
text(26, .93, sprintf('n = %i', msCounts(3)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(26, .95, sprintf('n = %i', noMsCounts(3)), 'FontWeight', 'bold' ,'FontSize', 12)
text(35, .93, sprintf('n = %i', msCounts(4)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(35, .95, sprintf('n = %i', noMsCounts(4)), 'FontWeight', 'bold' ,'FontSize', 12)
text(38, .88, sprintf('n = %i', msCounts(5)), 'Color', rgb('DarkCyan') ,'FontWeight','bold' ,'FontSize',12)
text(38, .90, sprintf('n = %i', noMsCounts(5)), 'FontWeight', 'bold' ,'FontSize', 12)

end