function plot_trial_dprime(v_dprime, ci_dprime, splitLocation, titleStr)

if splitLocation
    xLim = -45;
    distances = [-41, -38, -29, -16, 0, 16, 29, 38, 41];
else
    xLim = -5;
    distances = [0, 16, 29, 38, 41];
end

errorbar(distances, v_dprime(1:end-1), ci_dprime(1:end-1), 'ok', 'LineStyle', 'none', 'LineWidth', 2)
hold on
plot([xLim 45], [v_dprime(end), v_dprime(end)] , 'r', 'LineWidth',2)
plot([xLim 45], [v_dprime(end) + ci_dprime(end),  v_dprime(end) + ci_dprime(end)] , 'r--', 'LineWidth',2)
plot([xLim 45], [v_dprime(end) - ci_dprime(end), v_dprime(end) - ci_dprime(end)] , 'r--', 'LineWidth',2)
title(titleStr)
set(gca, 'FontSize', 15, 'YTick', -10:1:10)
xticks(distances)
ylabel("Sensitivity (d')")
xlabel('Distance from saccade cue location')
ylim([floor(min(v_dprime) - max(ci_dprime)), ceil(max(v_dprime) + max(ci_dprime))])
xlim([xLim 45])
box off

end