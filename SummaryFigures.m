% D-prime Periphery
figure('Position', [300, 200, 600, 600]);
trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral'};
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    v_dprime(trial_idx) = summary.(cur_trial).dprime;
    ci_dprime(trial_idx) = summary.(cur_trial).ci_dprime;
end   
plot_trial_dprime(v_dprime, ci_dprime, 0, 'Peripheral')

print('-djpeg',sprintf('./Documents/Figures/Summary/DPrime%s.jpeg', ''));
print('-depsc',sprintf('./Documents/EPSFigures/Summary/DPrime%s.epsc', ''));

% D-prime Periphery
figure('Position', [300, 200, 600, 600]);
trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral'};
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    v_dprime(trial_idx) = summary.(cur_trial).dprime;
    ci_dprime(trial_idx) = summary.(cur_trial).ci_dprime;
end   
plot_trial_dprime(v_dprime, ci_dprime, 0, 'Peripheral (No Microsaccade Performed)')
hMs = findobj(gcf, 'Color','k');
hMs.Color = [0 0 1];
hold on
trial_types = {'no_ms_0', 'no_ms_1', 'no_ms_2', 'no_ms_3', 'no_ms_4', 'neutral'};
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    v_dprime(trial_idx) = summary.(cur_trial).dprime;
    ci_dprime(trial_idx) = summary.(cur_trial).ci_dprime;
end 
plot_trial_dprime(v_dprime, ci_dprime, 0, 'Peripheral (No Microsaccade Performed)')
hNoMs = findobj(gcf, 'Color','k');
legend([hMs hNoMs], {'Ms trials', 'No Ms trials'})

print('-djpeg',sprintf('./Documents/Figures/Summary/NoMs_DPrime%s.jpeg', ''));
print('-depsc',sprintf('./Documents/EPSFigures/Summary/NoMs_DPrime%s.epsc', ''));

% D-prime Fixation
figure('Position', [300, 200, 300, 600]);
trial_types = {'cong_neutral','incong_neutral'};
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    v_dprime(trial_idx) = summary.(cur_trial).dprime;
    ci_dprime(trial_idx) = summary.(cur_trial).ci_dprime;
end   
plot_fixation_dprime(v_dprime, ci_dprime, 'Fixation', {'Valid','Invalid'})

print('-djpeg',sprintf('./Documents/Figures/Summary/Fixation_DPrime%s.jpeg', ''));
print('-depsc',sprintf('./Documents/EPSFigures/Summary/Fixation_DPrime%s.epsc', ''));

% Distance from target (sacc cue)
indv.dist_from_target{1,:} = cell2mat(indv.gaze_during_sacc_1.dist_from_target);
indv.dist_from_target{2,:} = cell2mat(indv.gaze_during_sacc_3.dist_from_target);
indv.dist_from_target{3,:} = cell2mat(indv.gaze_during_sacc_5.dist_from_target);
indv.dist_from_target{4,:} = cell2mat(indv.gaze_during_sacc_7.dist_from_target);
indv.dist_from_target{5,:} = cell2mat(indv.gaze_during_sacc_0.dist_from_target);
figure;
MultipleHist(indv.dist_from_target, 'binfactor', .75, 'samebins','smooth','color','parula');
legend('Sacc. Up','Sacc. Left','Sacc. Down','Sacc. Right','Neutral')
title('Distance from Target')
print('-djpeg', './Documents/Figures/Summary/distanceFromTargetHist.jpeg');
print('-depsc', './Documents/EPSFigures/Summary/distanceFromTargetHist.epsc');

for resp_cue = 1:9
    trial_name = sprintf('gaze_during_resp_%i', resp_cue);
    indv.dist_from_target_resp{resp_cue,:} = cell2mat(indv.(trial_name).dist_from_target);
end
figure;
MultipleHist(indv.dist_from_target_resp, 'binfactor', 1.1, 'samebins','smooth','color','parula');
legend({'1','2','3','4','5','6','7','8','9'})
title('Distance from Target')
print('-djpeg', './Documents/Figures/Summary/distanceFromTargetHist.jpeg');
print('-depsc', './Documents/EPSFigures/Summary/distanceFromTargetHist.epsc');


% Dprime Periphary (Split)
load('./Data/across_subjects_high_performance_split.mat')
trial_types = {'minus_4', 'minus_3', 'minus_2', 'minus_1', 'minus_0','plus_1','plus_2','plus_3', 'minus_4', 'neutral'};
figure('Position', [300, 200, 1100, 600]);
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    v_dprime(trial_idx) = summary.(cur_trial).dprime;
    ci_dprime(trial_idx) = summary.(cur_trial).ci_dprime;
end   
plot_trial_dprime(v_dprime, ci_dprime, 1, 'Peripheral')
print('-djpeg',sprintf('./Documents/Figures/Summary/DPrime%s.jpeg', '_Split'));
print('-depsc',sprintf('./Documents/EPSFigures/Summary/DPrime%s.epsc', '_Split'));

% Heatmap Peripheral
 trial_types = {'minus_0','plus_1','plus_2','plus_3','minus_4', 'minus_3', 'minus_2', 'minus_1', 'incong_neutral'};
for trial_idx = 1:length(trial_types)
    cur_trial = trial_types{trial_idx};
    dprime_vector(trial_idx) = summary.(cur_trial).dprime;
    perf_vector(trial_idx) = summary.(cur_trial).avg_perf;
end            
plot_sensitivity_map(dprime_vector, [], 'Peripheral')
print('-djpeg',sprintf('./Documents/Figures/Summary/Heatmap%s.jpeg', '_Split'));
print('-depsc',sprintf('./Documents/EPSFigures/Summary/Heatmap%s.epsc', '_Split'));

% Heatmap Interpolated Peripheral         
plot_interp_sensitivity(real(log(dprime_vector*100)), 'Peripheral')
axis off
title('')
print('-dbmp',sprintf('./Documents/Figures/Summary/InterpolatedHeatmap%s.bmp', '_Split'));
print('-depsc',sprintf('./Documents/EPSFigures/Summary/InterpolatedHeatmap%s.epsc', '_Split'));


