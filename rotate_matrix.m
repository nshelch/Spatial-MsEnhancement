
function [rotated_x, rotated_y] = rotate_matrix(x, y, saccCue)

% figure;
% plot([0, 0, 0, 30, -30], [0, 30, -30, 0, 0], 'sw','MarkerEdgeColor','k', 'MarkerSize',10)
% axis([-40 40 -40 40])
% hold on

switch saccCue
    case 0
        theta = 0;
        dx = 0;
        dy = 0;
    case 1
        theta = 0;
        dx = 0;
        dy = 0;
    case 3
        theta = pi/2;
        dx = 0;
        dy = 30;
    case 5
        theta = pi;
        dx = 0;
        dy = 30;
    case 7
        theta = (3*pi)/2;
        dx = 0;
        dy = 30;
end

R = [cos(theta) -sin(theta); sin(theta) cos(theta)];
rotated_coord = R*[x;y];
rotated_x = rotated_coord(1); rotated_y = rotated_coord(2);

% plot(rotated_x, rotated_y, 'r.','MarkerSize', 25)

end