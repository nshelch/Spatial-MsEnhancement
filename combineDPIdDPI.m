subjectName = {'Z085'};
rgbValue = 75;
splitLoc = 0;


load(sprintf('./Data/%s_RGB%i_summary.mat', subjectName{1}, rgbValue))
dpiFt = ft;

load(sprintf('./Data/%s_RGB%i_DDPI_summary.mat', subjectName{1}, rgbValue))
ddpiFt = ft;

switch splitLoc
    case 1
        trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral', ...
            'plus_1','plus_2','plus_3', 'cong_neutral', 'incong_neutral', 'no_ms_0', ...
            'neutral_1', 'neutral_2', 'neutral_3', 'neutral_4', 'neutral_5', ...
            'neutral_6', 'neutral_7', 'neutral_8', 'neutral_9'};
    case 0
        trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral', ...
            'cong_neutral', 'incong_neutral', 'no_ms_0', ...
            'neutral_1', 'neutral_2', 'neutral_3', 'neutral_4', 'neutral_5', ...
            'neutral_6', 'neutral_7', 'neutral_8', 'neutral_9'};      
end

for tIdx = 1:length(trial_types)
    ft.(trial_types{tIdx}).perf = [dpiFt.(trial_types{tIdx}).perf, ddpiFt.(trial_types{tIdx}).perf];
    ft.(trial_types{tIdx}).response_time = [dpiFt.(trial_types{tIdx}).response_time, ddpiFt.(trial_types{tIdx}).response_time];
    ft.(trial_types{tIdx}).subj_resp = [dpiFt.(trial_types{tIdx}).subj_resp, ddpiFt.(trial_types{tIdx}).subj_resp];
    ft.counter.(trial_types{tIdx}) = dpiFt.counter.(trial_types{tIdx}) + ddpiFt.counter.(trial_types{tIdx});
    ft.(trial_types{tIdx}).orientation_cued_target = [dpiFt.(trial_types{tIdx}).orientation_cued_target, ddpiFt.(trial_types{tIdx}).orientation_cued_target];
    [stats.d_prime.d.(trial_types{tIdx}), ~, stats.d_prime.ci.(trial_types{tIdx}), stats.d_prime.crit.(trial_types{tIdx}), stats.d_prime.var.(trial_types{tIdx})] = ...
        CalculateDprime_2(ft.(trial_types{tIdx}).subj_resp, ft.(trial_types{tIdx}).orientation_cued_target);
end

if length(ft.minus_0.perf) > 30 && length(ft.no_ms_0.perf) > 30
    fprintf('\nTesting Congruent Ms vs Congruent No Ms Trials\n')
    [~, ~, ~, ~, p_ms_v_noms] = Z_Test(sum(ft.minus_0.perf), length(ft.minus_0.perf), ...
        sum(ft.no_ms_0.perf), length(ft.no_ms_0.perf));
    stats.z_test.p_ms_v_noms = p_ms_v_noms;   
end
% load(sprintf('./Data/%s_RGB%i_combinedData.mat', subjectName{1}, rgbValue), 'ft','stats')
