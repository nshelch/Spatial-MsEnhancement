function [graphHandle, emHist] = plotEmTuningCurve(varargin)
% Must require at least one input which is the vector of angles used for
% plotting the tuning curve
%
% Other possible inputs:
% tuningAngles: the angles you want to use for the tuning curves. Default
% is 0 -> 360 in 45 deg intervals
% color: default is black. Uses the rgb function which can be downloaded
% here: https://www.mathworks.com/matlabcentral/fileexchange/24497-rgb-triple-of-color-name-version-2?focused=5124709&tab=function
% LineWidth: width of the line, default is 1
% LineStyle: default is solid
% plotSEM: states whether or not to plot the sem on the graph

% Checks to see that there is at least 1 input
narginchk(1, inf)

% Gets the angles out of varargin
emAngles = varargin{1};

% Sets defaults for the graph
plotParams.Color = [0,0,0];
plotParams.LineStyle = '-';
plotParams.LineWidth = 1;
tuningAngles = linspace(0, 2*pi, 9);
plotParams.sem = 0;

for ii = 2:2:length(varargin)
    curVar = lower(varargin{ii});
    switch curVar
        case 'color'
            plotParams.Color = varargin{ii + 1};
        case 'linestyle'
            plotParams.LineStyle = varargin{ii + 1};
        case 'linewidth'
            plotParams.LineWidth = varargin{ii + 1};
        case 'tuningangles'
            tuningAngles = varargin{ii + 1};
        case 'plotsem'
            plotParams.sem = 1;
    end
end

angleVector = cat(2, emAngles{:}); % direction vector (rho)
angleHist = hist(angleVector, tuningAngles);
angleHist(1) = angleHist(1) + angleHist(end); % combine 0 and 2*pi
angleHist = angleHist(1:end-1);% combine 0 and 2*pi
angleHist = angleHist / sum(angleHist); % normalizes the histogram
emHist.average = sum(exp(1i*angleHist)); % radial mean
emHist.sem = [angle(emHist.average), sqrt(-2*log(abs(emHist.average)/length(angleHist)))]; % radial std
emHist.data = angleHist;

graphHandle = polar(tuningAngles, [angleHist(1, :), angleHist(1, 1)]);
graphHandle.Color = plotParams.Color;
graphHandle.LineWidth = plotParams.LineWidth;
graphHandle.LineStyle = plotParams.LineStyle;

if plotParams.sem
    lo = emHist.average - emHist.sem;
    hi = emHist.average + emHist.sem;
    [xlo1, ylo1] = pol2cart(emHist.average, [lo, lo(1)]);
    [xhi1, yhi1] = pol2cart(emHist.average, [hi, hi(1)]);
    hold all;
    hf1 = nan(length(xlo1) - 1, 1);
    for jj = 1:(length(xlo1)-1)
        hf1(jj) = fill([xlo1(jj:jj+1) xhi1(jj+1:-1:jj)], [ylo1(jj:jj+1) yhi1(jj+1:-1:jj)],...
            'k'); 
    end
    set(hf1, 'FaceAlpha', .3, 'EdgeColor', 'none');
    set(hf1, 'FaceColor', plotParams.Color);
end

%%% Use for across subjects
% lo = comData.fixation.saccades.directionHist(1, :)...
%     - comData.fixation.saccades.directionHist(2, :);
% hi = comData.fixation.saccades.directionHist(1, :)...
%     + comData.fixation.saccades.directionHist(2, :);
% [xlo1, ylo1] = pol2cart(paramsPP.saccades.direction, [lo, lo(1)]);
% [xhi1, yhi1] = pol2cart(paramsPP.saccades.direction, [hi, hi(1)]);
% hold all;
% % hf1 = nan(length(xlo1)-1, 1);
% hf2 = nan(length(xlo) - 1, 1);
% for jj = 1:(length(xlo1)-1)
%     hf1(jj) = fill([xlo1(jj:jj+1) xhi1(jj+1:-1:jj)], [ylo1(jj:jj+1) yhi1(jj+1:-1:jj)],...
%         'k');
%     hf2(jj) = fill([xlo(jj:jj+1) xhi(jj+1:-1:jj)], [ylo(jj:jj+1) yhi(jj+1:-1:jj)],...
%         'b');
% end
% set([hf1, hf2], 'FaceAlpha', .3, 'EdgeColor', 'none');
% set(hf1, 'FaceColor', [.7 .7 .7]);
% set(plr2, 'Color', [.7 .7 .7]);
% set([plr1, plr2], 'LineWidth', 2);
end