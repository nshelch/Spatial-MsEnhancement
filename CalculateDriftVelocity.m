function [Speed,Velocity] = CalculateDriftVelocity(drifts,TimeInterval)

% Calculates drift velocity and speed given x and y positions and a time
% interval. Velocity and speed are calculated using sgfilt.


if ~( length(TimeInterval) == 2 || length(TimeInterval) == 1 )
    error('Time interval not valid')
end

for ii = 1:length(drifts)
    
    if length(TimeInterval) == 2
        tmp_x = sgfilt(drifts(ii).x(TimeInterval(1):TimeInterval(2)),3,41,1);
        tmp_y = sgfilt(drifts(ii).y(TimeInterval(1):TimeInterval(2)),3,41,1);
        x = tmp_x(floor((41/2)+10):end-floor(41/2));
        y = tmp_y(floor((41/2)+10):end-floor(41/2));
        vel_tmp{ii} = sqrt(x .^2 + y .^ 2) * 1000;
        speed_tmp(ii,:) = sqrt(x .^ 2+ y .^ 2) * 1000;
    else
        tmp_x = sgfilt(drifts(ii).x(TimeInterval(1):end),3,41,1);
        tmp_y = sgfilt(drifts(ii).y(TimeInterval(1):end),3,41,1);
        x = tmp_x(floor((41/2)+10):end-floor(41/2));
        y = tmp_y(floor((41/2)+10):end-floor(41/2));
        vel_tmp{ii} = sqrt(x .^2 + y .^ 2) * 1000;
        
    end
    
end

Velocity = nanmean(cell2mat(vel_tmp));

if length(TimeInterval) == 2
    Speed.avg = nanmean(speed_tmp);
    Speed.SE = nanstd(speed_tmp)/sqrt(length(speed_tmp));
else
    Speed = [];
end

end
