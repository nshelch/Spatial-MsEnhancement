function GenerateMap(result, limit, pxAngle)

% figure
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
% set(gcf, 'Colormap', parula)

hold on
pcolor(linspace(-limit, limit, size(result, 1)), linspace(-limit, limit, size(result, 1)), result');
% imgaussfilt(f)
plot([-limit limit], [0 0], '--k')
plot([0 0], [-limit limit], '--k')

set(gca, 'FontSize', 12) 
xlabel('X [arcmin]')
ylabel('Y [arcmin]')
axis tight
axis square
caxis([floor(min(min(result))) ceil(max(max(result)))])
colorbar
shading interp;

clear figure

