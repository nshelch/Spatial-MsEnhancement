clear; clc;

subjects = {'NS','Z008','Z031','Z066','Z085'};
rgb_value = [90, 15, 100, 50, 75];
splitLocation = 0;
n_bins = 50;

switch splitLocation
    case 1
        trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral', ...
            'plus_1','plus_2','plus_3', 'cong_neutral', 'incong_neutral', 'no_ms_0', ...
            'no_ms_1','no_ms_2','no_ms_3','no_ms_4'};
    case 0
        trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral', ...
            'cong_neutral', 'incong_neutral', 'no_ms_0'};
end

[indv.saccCue1Heatmap.landing, indv.saccCue3Heatmap.landing, indv.saccCue5Heatmap.landing, indv.saccCue7Heatmap.landing, ...
    indv.saccCue1Heatmap.starting, indv.saccCue3Heatmap.starting, indv.saccCue5Heatmap.starting, indv.saccCue7Heatmap.starting] = deal(zeros(n_bins,n_bins));

% Combining indivudual subject data
for ii = 1:length(subjects)
    
    switch splitLocation
        case 1
            load(sprintf('./Data/%s_RGB%i_split_summary.mat', subjects{ii}, rgb_value(ii)))
        case 0
            load(sprintf('./Data/%s_RGB%i_summary.mat', subjects{ii}, rgb_value(ii)))
    end
    % Statistical Tests
    for trial_idx = 1:length(trial_types)
        trial = trial_types{trial_idx};
        indv.(trial).dprime(ii) = stats.d_prime.d.(trial);
        indv.(trial).crit_dprime(ii) = stats.d_prime.crit.(trial);
        indv.(trial).ci_dprime(ii) = stats.d_prime.ci.(trial);
        indv.(trial).dist_from_target(ii) = nanmean(cell2mat(ft.(trial).dist_from_target));
        indv.(trial).avg_perf(ii) = ft.(trial).avg_perf;
        indv.(trial).avg_ms_amp(ii) = nanmean(ft.(trial).amp);
        indv.(trial).resp_time(ii) = nanmean(ft.(trial).response_time);
        indv.(trial).count(ii) = ft.counter.(trial);
    end
    
    % Temporal Dynamics
    ft.cong.id = ft.minus_0.id;
    ft.cong.ms_latency = ft.minus_0.ms_latency;
    ft.cong.resp_time = ft.minus_0.response_time;
    ft.incong.id = [ft.minus_1.id, ft.minus_2.id, ft.minus_3.id, ft.minus_4.id];
    ft.incong.ms_latency = [ft.minus_1.ms_latency, ft.minus_2.ms_latency, ft.minus_3.ms_latency, ft.minus_4.ms_latency];
    ft.incong.resp_time = [ft.minus_1.response_time, ft.minus_2.response_time, ft.minus_3.response_time, ft.minus_4.response_time];
    indv = temporalDynamics(ii, ft, indv, 2);
    indv.cong_no_ms.dprime(ii) = stats.d_prime.d.no_ms_0;
    incongNoMsId = [ft.no_ms_1.id, ft.no_ms_2.id, ft.no_ms_3.id, ft.no_ms_4.id];
    [indv.incong_no_ms.dprime(ii), ~, ~, ~, ~] = CalculateDprime_2(ft.subj_resp(incongNoMsId), ft.orientation_cued_target(incongNoMsId));
    
    if splitLocation
        indv.avg_ms_latency{ii} = [ft.ms_initiation(ft.minus_0.id), ft.ms_initiation(ft.minus_1.id), ...
            ft.ms_initiation(ft.minus_2.id), ft.ms_initiation(ft.minus_3.id), ft.ms_initiation(ft.minus_4.id), ...
            ft.ms_initiation(ft.plus_1.id), ft.ms_initiation(ft.plus_2.id), ft.ms_initiation(ft.plus_3.id)];
    else
        indv.avg_ms_latency{ii} = [ft.ms_initiation(ft.minus_0.id), ft.ms_initiation(ft.minus_1.id), ...
            ft.ms_initiation(ft.minus_2.id), ft.ms_initiation(ft.minus_3.id), ft.ms_initiation(ft.minus_4.id)];
    end
    
    % Distance from target based on saccade cue
    for sacc_cue = [0,1,3,5,7]
        trial_name = sprintf('gaze_during_sacc_%i', sacc_cue);
        overlap = intersect(find(ft.sacc_cue_type == sacc_cue), ft.valid_trial_id);
        indv.(trial_name).dist_from_target{ii} = ft.avg_dist_from_target(overlap);
    end
    
    for resp_cue = 1:9
        valid_trials = [ft.minus_0.id, ft.minus_1.id, ft.minus_2.id, ft.minus_3.id, ft.minus_4.id];
        trial_name = sprintf('gaze_during_resp_%i', resp_cue);
        if resp_cue == 9
            valid_trials = [ft.cong_neutral.id];
            overlap = intersect(find(ft.cue_location == resp_cue), valid_trials);
        else
            overlap = intersect(find(ft.cue_location == resp_cue), valid_trials);
        end
        indv.(trial_name){ii} = ft.avg_dist_from_target(overlap);
        
    end
    
    %     for saccIdx = [1,3,5,7]
    %         limit = 30;
    %         indvLabel = sprintf('saccCue%iHeatmap', saccIdx);
    %         ftLabel = sprintf('sacc_cue_%i_landing', saccIdx);
    %         result = histogram2(ft.heatmap.(ftLabel).xx(:)', ft.heatmap.(ftLabel).yy(:)', [-limit, limit, n_bins; -limit, limit, n_bins]);
    %         result = result./max(max(result));
    %         indv.(indvLabel).landing = indv.(indvLabel).landing  + result;
    %
    %         ftLabel = sprintf('sacc_cue_%i_target', saccIdx);
    %         result = histogram2(ft.heatmap.(ftLabel).xx(:)', ft.heatmap.(ftLabel).yy(:)', [-limit, limit, n_bins; -limit, limit, n_bins]);
    %         result = result./max(max(result));
    %         indv.(indvLabel).starting  = indv.(indvLabel).starting  + result;
    %     end
    
    nonvalid_trial_id = [ft.minus_1.id, ft.minus_2.id, ft.minus_3.id, ft.minus_4.id];
    indv.nonvalid_trial_perf{ii} = [ft.minus_1.perf, ft.minus_2.perf, ft.minus_3.perf, ft.minus_4.perf];
    [indv.nonvalid_trial_dprime(ii), ~, indv.nonvalid_trial_ci(ii), ~, ~] = ...
        CalculateDprime_2(ft.subj_resp(nonvalid_trial_id), ft.orientation_cued_target(nonvalid_trial_id));
    
end

trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral', ...
    'cong_neutral', 'incong_neutral', 'no_ms_0', 'cong','incong','cong_no_ms','incong_no_ms'};
% Creating summary structure
for ii = 1:length(trial_types)
    trial = trial_types{ii};
    var_names = fieldnames(indv.(trial));
    for var_idx = 1:length(var_names)
        cur_var = var_names{var_idx};
        if ~strcmp(cur_var, 'drift') && ~strcmp(cur_var, 'trialIdx')
            summary.(trial).(cur_var) = nanmean(indv.(trial).(cur_var));
            summary.(trial).(sprintf('se_%s', cur_var)) = nansem(indv.(trial).(cur_var), length(subjects));
        elseif strcmp(cur_var, 'drift')
            drift_var = fieldnames(indv.(trial).drift);
            for drift_var_idx = 1:length(drift_var)
                cur_drift_var = drift_var{drift_var_idx};
                summary.(trial).drift.(cur_drift_var) = nanmean(indv.(trial).drift.(cur_drift_var));
                summary.(trial).drift.(sprintf('se_%s', cur_drift_var)) = nansem(indv.(trial).drift.(cur_drift_var), length(subjects));
            end
        end
    end
end

for saccIdx = [1,3,5,7]
    indvLabel = sprintf('saccCue%iHeatmap', saccIdx);
    indv.(indvLabel).landing = indv.(indvLabel).landing./length(subjects);
    indv.(indvLabel).landing = indv.(indvLabel).landing./max(max(indv.(indvLabel).landing));
    indv.(indvLabel).landing(indv.(indvLabel).landing == 0) = NaN;
    
    indv.(indvLabel).starting = indv.(indvLabel).starting./length(subjects);
    indv.(indvLabel).starting = indv.(indvLabel).starting./max(max(indv.(indvLabel).starting));
    indv.(indvLabel).starting(indv.(indvLabel).starting == 0) = NaN;
end

var_names = fieldnames(indv);
for ii = 1:length(var_names)
    cur_var = var_names{ii};
    if ~any(strcmp(cur_var, trial_types))
        if ~iscell(indv.(cur_var))
            summary.(cur_var) = nanmean(indv.(cur_var));
            summary.(sprintf('se_%s',cur_var)) = nansem(indv.(cur_var));
        end
    end
end

switch splitLocation
    case 1
        save('./Data/across_subjects_split.mat','summary','indv')
    case 0
        lsave('./Data/across_subjects.mat','summary','indv')
end



