function [trial_type, counter] = trial_type_classification(ms, splitLocation, xPos, yPos, respCue, saccCue, counter)

trial_type = 'other';
% If a microsaccade has been performed
if ms    
    % Non-neutral SaccCue + Non-neutral RespCue
    if saccCue ~= 0 && respCue ~= 9 
        % Microsaccade == SaccCue
        if check_ms_sacc_cue(xPos, yPos, saccCue)
            abs_dist_away = min(mod(saccCue - respCue, 8), mod(respCue - saccCue, 8));
            % Splits the groupings into left and right distance away
            if splitLocation
                dist_away = mod(saccCue - respCue, 8);
                if dist_away >= 0 && dist_away <= 4
                    trial_type = sprintf('minus_%i', abs_dist_away);
                else
                    trial_type = sprintf('plus_%i', abs_dist_away);
                end
            % Creates categories based on the absolute distance away
            else
                trial_type = sprintf('minus_%i', abs_dist_away);
            end
            % Microsaccade == RespCue + RespCue ~= SaccCue
        elseif check_ms_resp_cue(xPos, yPos, respCue) && ...
                respCue ~= saccCue
            trial_type = 'valid_incong';
            % Microsaccade != RespCue + RespCue == SaccCue
        elseif check_ms_resp_cue(xPos, yPos, respCue) && ...
                respCue == saccCue
            trial_type = 'invalid_cong';
        end
        % Neutral SaccCue + Non-neutral RespCue
    elseif saccCue == 0 && respCue ~= 9
        % Microsaccade == RespCue
        if check_ms_resp_cue(xPos, yPos, respCue)
            trial_type = 'neutral_cong_ms';
            % Microsaccade != RespCue
        elseif ~check_ms_resp_cue(xPos, yPos, respCue)
            trial_type = 'neutral_incong_ms';
        end
        % Non-neutral SaccCue + Neutral RespCue
    elseif saccCue ~= 0 && respCue ==9
        % Microsaccade == SaccCue
        if check_ms_sacc_cue(xPos, yPos, saccCue)
            trial_type = 'incong_neutral';
        end
    end
    
    % If no microsaccade has been performed
else
    
    % Neutral SaccCue + Peripheral RespCue
    if saccCue == 0 && respCue ~= 9
        trial_type = 'neutral';
    % Neutral SaccCue + Neutral RespCue
    elseif saccCue == 0 && respCue == 9
        trial_type = 'cong_neutral';
    % Peripheral SaccCue + Peripheral RespCue
    else
        abs_dist_away = min(mod(saccCue - respCue, 8), mod(respCue - saccCue, 8));
        trial_type = sprintf('no_ms_%i', abs_dist_away);
    end
end

counter.(trial_type) = counter.(trial_type) + 1;

end

function output = check_ms_sacc_cue(xPos, yPos, saccCue)
% Checks whether the microsaccade was heading in the direction of the
% saccade cue
if (yPos > 0 && saccCue == 1) || ...
        (xPos > 0 && saccCue == 3) || ...
        (yPos < 0 && saccCue == 5) || ...
        (xPos < 0 && saccCue == 7)
    output = 1;
else
    output = 0;
end
end

function output = check_ms_resp_cue(xPos, yPos, respCue)
% Checks whether the microsaccade was heading in the direction of the
% response cue
if (yPos > 0 && respCue == 1) || ...
        (xPos > 0 && yPos > 0 && respCue == 2) || ...
        (xPos > 0 && respCue == 3) || ...
        (xPos > 0 && yPos < 0 && respCue == 4) || ...
        (yPos < 0 && respCue == 5) || ...
        (xPos < 0 && yPos <0 && respCue == 6) || ...
        (xPos < 0 && respCue == 7) || ...
        (xPos < 0 && yPos > 0 && respCue == 8)
    output = 1;
else
    output = 0;
end
end