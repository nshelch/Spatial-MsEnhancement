function plot_distance_from_target(ft, saccCue, respCue)
% Looks at the gaze location during target presentation and plot the
% distance from the target grouped by saccade cue (saccCue = 1), response
% cue (respCue = 1), or both (saccCue = 1, respCue = 1)

if saccCue
    f1 = figure(300);
    f1.Position = [500, 300, 1000, 600];
    subplot(1,2,1)
    hold on;
    color_vector = parula(5);
    h = plot(0,0,'color',color_vector(1,:),'Linewidth', 5); j = plot(0,0,'color',color_vector(2,:),'Linewidth', 5); k = plot(0,0,'color',color_vector(3,:),'Linewidth', 5);
    l = plot(0,0,'color',color_vector(4,:),'Linewidth', 5); m = plot(0,0,'color',color_vector(5,:),'Linewidth', 5);
    q = plot_stimuli_location(0, 11*0.6848, 30*0.6848);
    id = 1;
    for sacc_cue = [1, 3, 5, 7, 0]
        cue{1,:} = ft.avg_dist_from_target(intersect(find(ft.sacc_cue_type == sacc_cue), ft.valid_trial_id));
        overlap = intersect(find(ft.sacc_cue_type == sacc_cue), ft.valid_trial_id);
        q = plot(ft.dist_from_target_x(overlap), ft.dist_from_target_y(overlap), 'o', ...
            'MarkerFaceColor', color_vector(id,:), 'MarkerEdgeColor', 'none','MarkerSize',4);
        id = id + 1;
    end
    set(q, 'HandleVisibility', 'off')
    legend([h,j,k,l,m], {'Sacc. Up','Sacc. Left','Sacc. Down', 'Sacc. Right','Neutral'})
    title('Distance from target (Combined)')

    % Combined by Saccade Cue
    cue{1,:} = ft.avg_dist_from_target(intersect(find(ft.sacc_cue_type == 1), ft.valid_trial_id));
    cue{2,:} = ft.avg_dist_from_target(intersect(find(ft.sacc_cue_type == 3), ft.valid_trial_id));
    cue{3,:} = ft.avg_dist_from_target(intersect(find(ft.sacc_cue_type == 5), ft.valid_trial_id));
    cue{4,:} = ft.avg_dist_from_target(intersect(find(ft.sacc_cue_type == 7), ft.valid_trial_id));
    cue{5,:} = ft.avg_dist_from_target(intersect(find(ft.sacc_cue_type == 0), ft.valid_trial_id));
    subplot(1,2,2)
    MultipleHist(cue, 'binfactor', .75, 'samebins','smooth','color','parula');
    legend('Sacc. Up','Sacc. Left','Sacc. Down','Sacc. Right','Neutral','Location','Best')
    title('Distance from Target')
end        
if respCue
    for resp_cue = 1:9
        valid_trials = [ft.minus_0.id, ft.minus_1.id, ft.minus_2.id, ft.minus_3.id, ft.minus_4.id];
        trial_name = sprintf('gaze_during_resp_%i', resp_cue);
        if resp_cue == 9
            valid_trials = [ft.cong_neutral.id];
            overlap = intersect(find(ft.cue_location == resp_cue), ft.valid_trial_id);
        else
            overlap = intersect(find(ft.cue_location == resp_cue), ft.valid_trial_id);
        end
        dist_from_target_resp{resp_cue} = ft.avg_dist_from_target(overlap);
        
    end
    figure(400);
    MultipleHist(dist_from_target_resp, 'binfactor', 1.1, 'samebins','smooth','color','parula');
    legend({'1','2','3','4','5','6','7','8','9'})
    title('Distance from Target')
end    
if saccCue && respCue
    f2 = figure(500);
    f2.Units = 'normalized';
    f2.OuterPosition = [0 0 1 1];
    subplot_counter = 0;
    for sacc_cue = [0,1,3,5,7]
        for resp_cue = 1:9
            set(gca,'BoxStyle','full','Box','on')
            subplot_counter = subplot_counter + 1;
            subplot(5, 9, subplot_counter)
            sacc_trials = find(ft.sacc_cue_type == sacc_cue);
            resp_trials = find(ft.cue_location == resp_cue);
            overlap = intersect(intersect(sacc_trials, resp_trials), ft.valid_trial_id);
            indv_cue = ft.avg_dist_from_target(overlap);
            boxplot(indv_cue, 'color','b', 'orientation', 'horizontal');
            title(sprintf('Sacc: %i, Resp: %i \n(n = %i)', sacc_cue, resp_cue, length(overlap)))
            xlim([0 40])
        end
    end
    
    f3 = figure(600);
    f3.Units = 'normalized';
    f3.OuterPosition = [0 0 1 1];
    
    subplot_counter = 0;
    for sacc_cue = [0,1,3,5,7]
        for resp_cue = 1:9
            subplot_counter = subplot_counter + 1;
            subplot(5, 9, subplot_counter)
            plot_stimuli_location(0, 11*0.6848, 30*0.6848);
            hold on
            sacc_trials = find(ft.sacc_cue_type == sacc_cue);
            resp_trials = find(ft.cue_location == resp_cue);
            overlap = intersect(intersect(sacc_trials, resp_trials), ft.valid_trial_id);
            plot(ft.dist_from_target_x(overlap), ft.dist_from_target_y(overlap), 'b.','MarkerSize',10)
            title(sprintf('Sacc: %i, Resp: %i \n(n = %i)', sacc_cue, resp_cue, length(overlap)))
        end
    end
    
end

end