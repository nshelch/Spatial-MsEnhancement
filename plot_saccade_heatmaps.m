function plot_saccade_heatmaps(ft)

limit = 30;
subplot_idx = 0;
figure('units','normalized','outerposition',[0 0 1 1])
for sacc_cue = [0,1,3,5,7]
    subplot_idx = subplot_idx + 1;
%     sacc_target = sprintf('sacc_cue_%i_target', sacc_cue);
    sacc_landing = sprintf('sacc_cue_%i_landing', sacc_cue);
    switch sacc_cue
        case 0
            title_str = 'Sacc. Cue: Neutral';
        case 1
            title_str = 'Sacc. Cue: Up';
        case 3
            title_str = 'Sacc. Cue: Right';
        case 5
            title_str = 'Sacc. Cue: Down';
        case 7
            title_str = 'Sacc. Cue: Left';
    end
    
%     subplot(2, 5, subplot_idx)
%     plot_stimuli_location(0, 11*.6848, 30*.6848);
%     GenerateMap(ft.heatmap.(sacc_target).map, limit, .6848)
%     title(title_str)
%     hold on
    subplot(1, 5, subplot_idx)
    plot_stimuli_location(0, 11*.6848, 30*.6848);
    GenerateMap(ft.heatmap.(sacc_landing).map, limit, .6848)
    title(title_str)
    
    hold on
end
text(-200, 165, 'During Target Presentation','FontSize',20,'FontWeight','bold')
text(-190, 50, 'During Mask Period','FontSize',20,'FontWeight','bold')

end