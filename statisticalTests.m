function [stats] = statisticalTests(ft, trialTypes)

for trial_idx = 1:length(trialTypes)
    cur_trial = trialTypes{trial_idx};
    if ~isnan(ft.(cur_trial).perf)
        [stats.d_prime.d.(cur_trial), ~, stats.d_prime.ci.(cur_trial), stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = ...
            CalculateDprime_2(ft.subj_resp(ft.(cur_trial).id), ft.orientation_cued_target(ft.(cur_trial).id));
    else
        [stats.d_prime.d.(cur_trial), stats.d_prime.ci.(cur_trial), ...
            stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = deal(NaN);
        
    end
end

% Z Tests
% nonvalid_perf = [ft.minus_1.perf, ft.minus_2.perf, ft.minus_3.perf, ft.minus_4.perf];

%     fprintf('Testing Congruent vs Incongruent Trials\n')
%     [~, ~, ~, ~, p_cong_v_incong] = Z_Test(sum(ft.minus_0.perf), length(ft.minus_0.perf), ...
%         sum(nonvalid_perf), length(nonvalid_perf));
%

if length(ft.minus_0.perf) > 30 && length(ft.no_ms_0.perf) > 30
    fprintf('\nTesting Congruent Ms vs Congruent No Ms Trials\n')
    [~, ~, ~, ~, p_ms_v_noms] = Z_Test(sum(ft.minus_0.perf), length(ft.minus_0.perf), ...
        sum(ft.no_ms_0.perf), length(ft.no_ms_0.perf));
    stats.z_test.p_ms_v_noms = p_ms_v_noms;   
end

%     fprintf('\nTesting Neutral vs Congruent No Ms Trials\n')
%     [~, ~, ~, ~, p_neutral_v_noms] = Z_Test(sum(ft.neutral.perf), length(ft.neutral.perf), ...
%         sum(ft.no_ms_0.perf), length(ft.no_ms_0.perf));
%
%     fprintf('\nTesting Neutral vs Congruent Ms Trials\n')
%     [~, ~, ~, ~, p_neutral_v_cong] = Z_Test(sum(ft.neutral.perf), length(ft.neutral.perf), ...
%         sum(ft.minus_0.perf), length(ft.minus_0.perf));

%     stats.z_test.p_cong_v_incong = p_cong_v_incong;
%     stats.z_test.p_neutral_v_noms = p_ms_v_noms;

end