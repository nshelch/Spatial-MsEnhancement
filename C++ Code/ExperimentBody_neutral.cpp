#include "stdafx.h"
#include "ExperimentBody.h"
#include "MicrosaccadeEnhancement.h"
#include <math.h>

const float PI = 3.1415926353f;



///////////////////////////////////////////////////////////////////////////////////
ExperimentBody::ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params) :
	CExperiment(pxWidth, pxHeight, RefreshRate)
	
{
	setExperimentName("Spatial-MicrosaccadeEnhancement");
	m_paramsFile = Params;
	CMath::srand(time(NULL));
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::initialize()
{		
	CStabilizer::Instance()->enableSlowStabilization(true);
	
	N = 0;
	T = 0;
	L = 0;
	R = 0;
	U = 0;
	D = 0;


	InhibitionTest = m_paramsFile->getInteger(CFG_INHIBITION_TEST);

	// locations of boxes and targets
	TargetOffset = m_paramsFile->getFloat(CFG_TARGET_OFFSET);
	FixSize = m_paramsFile->getFloat(CFG_FIXSIZE);
	
	// define central fixation marker
	m_fixation = addObject(new CSolidPlane(255,255,255)); //255 for white, 0 for black
    m_fixation->pxSetSize(FixSize, FixSize);
	m_fixation->pxSetPosition(0,0);
	m_fixation->hide();

	// define trial fixation marker
	m_trial_fixation = addObject(new CSolidPlane(0, 0, 0));
	m_trial_fixation->pxSetSize(FixSize, FixSize);
	m_trial_fixation->pxSetPosition(0, 0);
	m_trial_fixation->hide();

	// define the cue
	m_cue = addObject(new CImagePlane("images/cue_big.tga"));
	m_cue->enableTrasparency(true);
	m_cue->pxSetPosition(0,0);
	m_cue->degSetAngle(90);
	
	//m_cue->pxSetSize(13, 13);
	m_cue->hide();
	
	BoxSize = m_paramsFile->getFloat(CFG_BOXSIZE);
	// define the place holders
	// change here to change the contrast of the boxes stimuli the background is 127, 255 white 0 black

	// Box labels: 
	//			1
	//		8		2
	//	7				3
	//		6		4
	//			5

	m_box1 = addObject(new CSolidPlane(150, 150, 150));
	m_box1->pxSetSize(BoxSize,BoxSize);
	m_box2 = addObject(new CSolidPlane(150, 150, 150));
	m_box2->pxSetSize(BoxSize, BoxSize);
	m_box3 = addObject(new CSolidPlane(150, 150, 150));
	m_box3->pxSetSize(BoxSize, BoxSize);
	m_box4 = addObject(new CSolidPlane(150, 150, 150));
	m_box4->pxSetSize(BoxSize, BoxSize);	
	m_box5 = addObject(new CSolidPlane(150, 150, 150));
	m_box5->pxSetSize(BoxSize, BoxSize);
	m_box6 = addObject(new CSolidPlane(150, 150, 150));
	m_box6->pxSetSize(BoxSize, BoxSize);
	m_box7 = addObject(new CSolidPlane(150, 150, 150));
	m_box7->pxSetSize(BoxSize, BoxSize);
	m_box8 = addObject(new CSolidPlane(150, 150, 150));
	m_box8->pxSetSize(BoxSize, BoxSize);
	m_box_fix = addObject(new CSolidPlane(150, 150, 150));
	m_box_fix->pxSetSize(BoxSize, BoxSize);

	m_box1->pxSetPosition(0, TargetOffset);
	m_box2->pxSetPosition((TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
	m_box3->pxSetPosition(TargetOffset, 0);
	m_box4->pxSetPosition((TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
	m_box5->pxSetPosition(0, -TargetOffset);
	m_box6->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
	m_box7->pxSetPosition(-TargetOffset, 0);
	m_box8->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
	m_box_fix->pxSetPosition(0, 0);

	// saccade cue
	m_cue_sacc = addObject(new CImagePlane("images/new_Sacc_cue.tga"));
	m_cue_sacc->enableTrasparency(true);
	m_cue_sacc->pxSetSize(30, 15);
	m_cue_sacc->pxSetPosition(0, 0);
	m_cue_sacc->hide();

	m_neutralcue_sacc = addObject(new CImagePlane("images/new_Sacc_neutralcue.tga"));
	m_neutralcue_sacc->enableTrasparency(true);
	m_neutralcue_sacc->pxSetSize(25, 25);
	m_neutralcue_sacc->pxSetPosition(0, 0);
	m_neutralcue_sacc->hide();
	
	m_box1->hide();
	m_box2->hide();
	m_box3->hide();
	m_box4->hide();
	m_box5->hide();
	m_box6->hide();
	m_box7->hide();
	m_box8->hide();
	m_box_fix->hide();
	
	// target is a small bar
	TSizeX = m_paramsFile->getInteger(CFG_TSIZEX);
	TSizeY = m_paramsFile->getInteger(CFG_TSIZEY);
	
	m_target1 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target1->pxSetSize(TSizeX, TSizeY);
	m_target1->pxSetPosition(0, TargetOffset);
	m_target1->hide();

	m_target2 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target2->pxSetSize(TSizeX, TSizeY);
	m_target2->pxSetPosition((TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
	m_target2->hide();
	
	m_target3 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target3->pxSetSize(TSizeX, TSizeY);
	m_target3->pxSetPosition(TargetOffset, 0);
	m_target3->hide();
	
	m_target4 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target4->pxSetSize(TSizeX, TSizeY);
	m_target4->pxSetPosition((TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
	m_target4->hide();
		
	m_target5 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target5->pxSetSize(TSizeX, TSizeY);
	m_target5->pxSetPosition(0, -TargetOffset);
	m_target5->hide();

	m_target6 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target6->pxSetSize(TSizeX, TSizeY);
	m_target6->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
	m_target6->hide();

	m_target7 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target7->pxSetSize(TSizeX, TSizeY);
	m_target7->pxSetPosition(-TargetOffset, 0);
	m_target7->hide();

	m_target8 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target8->pxSetSize(TSizeX, TSizeY);
	m_target8->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
	m_target8->hide();

	m_target9 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target9->pxSetSize(TSizeX, TSizeY);
	m_target9->pxSetPosition(0, 0);
	m_target9->hide();

	// performance keepers
	TotalCorrect = 0;
	TotalResponses = 0;
	neutral_correct = 0;
	InhibitionCounter = 0;

	// set the timers
	m_fixationTime = m_paramsFile->getFloat(CFG_FIXATION_TIME);
	m_targetTime = m_paramsFile->getFloat(CFG_TARGET_TIME);
	m_cueTime = m_paramsFile->getFloat(CFG_CUE_TIME);
	m_responseTime = m_paramsFile->getFloat(CFG_RESPONSE_TIME);
	m_maskTime = m_paramsFile->getFloat(CFG_MASK_TIME);
	m_delayTime = m_paramsFile->getFloat(CFG_DELAY_TIME);
	
	// set the pixel increment for the test calibration procedure during the exp
	// NOTE: smaller increments (in px) more precise is the recalibration but it will also take more time
	Increment = 1;
	ResponseFinalize = 0;
	xshift = 0;
	yshift = 0;
	xPos = 0;
	yPos = 0;
	TrialNumber = 1;
	m_numTestCalibration = 0;
	
	// set TestCalibration = 1 so that the experiment will start with a recalibration trial
	TestCalibration = 1;
	
	// boxes for the recalibration trials	
	m_whitecross = addObject(new CSolidPlane(255, 255, 255));
	m_whitecross->pxSetSize(10,10);
	m_redcross = addObject(new CSolidPlane(0, 0, 0));
	m_redcross->pxSetSize(10,10);

	disable(CExperiment::EIS_JP_STRT);
	disable(CExperiment::EIS_PHOTOCELL);
	disable(CExperiment::EIS_NOTRACK_ICON);
	disable(CExperiment::EIS_STAT1);
	
	
	hideAllObjects();
	m_state = STATE_LOADING;
	m_timer.start(1000);

	WAIT_RESPONSE = 1;
	
	/* Seed the random-number generator with current time so that
    * the numbers will be different every time we run.*/
	
    srand( (unsigned)time( NULL ) );


}
///////////////////////////////////////////////////////////////////////////////////
// write the progress file
void ExperimentBody::finalize()
{
			declareFinished();
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventRender(unsigned int FrameCount, CEOSData* Samples)
{

	float x;
	float y;

	switch (m_state) {
	case STATE_LOADING:


		COGLEngine::Instance()->clearScreen();
		glColor3d(255, 255, 255);

		// Set the background color for the experiment
		COGLEngine::Instance()->setBackgroundColor(127,127,127);

		// Copy the parameter file into the subject directory
		if (m_timer.isExpired())
		{
			char LocalDate[1024];
			time_t t = time(NULL);
			strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t));

			ostringstream DestinationFileName;
			DestinationFileName << m_paramsFile->getDirectory(CFG_DATA_DESTINATION) <<
				m_paramsFile->getString(CFG_SUBJECT_NAME) << "/" << m_paramsFile->getString(CFG_SUBJECT_NAME) <<
				"-" << LocalDate << "-params.cfg";;
			gotoFixation();
		}

		break;

	case STATE_TESTCALIBRATION:

		m_box1->hide();
		m_box2->hide();
		m_box3->hide();
		m_box4->hide();
		m_box5->hide();
		m_box6->hide();
		m_box7->hide();
		m_box8->hide();
		m_box_fix->hide();

		if (!m_timerCheck.isExpired())
		{
			CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Test Calibration, ResponseFinalize: %.0i", ResponseFinalize);
			if (!(ResponseFinalize == 1))
			{
				CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
				//CEnvironment::Instance()->outputMessage("State Test Calibration, %1.2f, %1.2f", x, y);
				moveToFront(m_redcross);
				m_redcross->pxSetPosition(x + xshift + xPos, y + yshift + yPos);
				m_redcross->show();
				m_whitecross->pxSetPosition(0, 0);
				m_whitecross->show();
/*
				CSolidPlane* leftUpperQuad;
				CSolidPlane* leftLowerQuad;
				CSolidPlane* rightUpperQuad;
				CSolidPlane* rightLowerQuad;
				leftUpperQuad = addObject(new CSolidPlane(0, 0, 0)); leftUpperQuad->pxSetSize(15, 15); leftUpperQuad->pxSetPosition(60, 60);
				leftLowerQuad = addObject(new CSolidPlane(255, 0, 0)); leftLowerQuad->pxSetSize(15, 15); leftLowerQuad->pxSetPosition(-60, 60);
				rightUpperQuad = addObject(new CSolidPlane(0, 255, 0)); rightUpperQuad->pxSetSize(15, 15); rightUpperQuad->pxSetPosition(60, -60);
				rightLowerQuad = addObject(new CSolidPlane(0, 0, 255)); rightLowerQuad->pxSetSize(15, 15); rightLowerQuad->pxSetPosition(-60, -60);*/

				

			}
			else
			{
				//CEnvironment::Instance()->outputMessage("State Test Calibration");
				TestCalibration = 0;

				xshift = xPos + xshift;
				yshift = yPos + yshift;
				//CEnvironment::Instance()->outputMessage("State Test Calibration, xshift: %.2f", xshift);
				//CEnvironment::Instance()->outputMessage("State Test Calibration, yshift: %.2f", yshift);
				//CEnvironment::Instance()->outputMessage("----------------------------------------------------------------");


				m_redcross->hide();
				m_whitecross->hide();

				endTrial();
				gotoFixation();

			}

		}
		break;

		//  STATE_FIXATION: the place holders are presented together with the central fixation point.
	case STATE_FIXATION:
		if (gate == 1)
		{
			CConverter::Instance()->a2p(Samples->x1, Samples->y1, Xlocation, Ylocation);
			//CEnvironment::Instance()->outputMessage("State Fixation, %1.2f, %1.2f", Xlocation, Ylocation);
			
			// to debug
			Xlocation = 0;
			Ylocation = 0;

			
			// Add fixation point
			m_trial_fixation->show();


			m_box1->pxSetPosition(0, TargetOffset);
			m_box2->pxSetPosition((TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
			m_box3->pxSetPosition(TargetOffset, 0);
			m_box4->pxSetPosition((TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
			m_box5->pxSetPosition(0, -TargetOffset);
			m_box6->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
			m_box7->pxSetPosition(-TargetOffset, 0);
			m_box8->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
			m_box_fix->pxSetPosition(0, 0);

			m_box1->hide();
			m_box2->hide();
			m_box3->hide();
			m_box4->hide();
			m_box5->hide();
			m_box6->hide();
			m_box7->hide();
			m_box8->hide();
			m_box_fix->hide();


			// time for fixation ON
			TimeFixationON = m_timerExp.getTime();
			//CEnvironment::Instance()->outputMessage("State Fixation");
			// fixation should stay on for one sec or as far as the gaze is stabilized
			m_timerfixation.start(m_fixationTime);
			gate = 0;
		}
		

		if (m_timerfixation.isExpired()){
			m_state = STATE_DELAY;
			m_box1->show();
			m_box2->show();
			m_box3->show();
			m_box4->show();
			m_box5->show();
			m_box6->show();
			m_box7->show();
			m_box8->show();
			m_box_fix->show();
			
			moveToFront(m_box1);
			moveToFront(m_box2);
			moveToFront(m_box3);
			moveToFront(m_box4);
			moveToFront(m_box5);
			moveToFront(m_box6);
			moveToFront(m_box7);
			moveToFront(m_box8);
			moveToFront(m_box_fix);

			// Picks a random delay for the beep after initial fixation time is over
			int beep_delayChoices[11] = { 500, 600, 700, 800, 900 };
			int beep_delay_id = rand() % 4;
			m_beepdelay = beep_delayChoices[beep_delay_id];
			m_timerbeep.start(m_beepdelay);

			// CEnvironment::Instance()->outputMessage("Delay Time: %i", m_beepdelay);
		}

		break;

		// STATE_DELAY: delay period 
		// show the directional or neutral cue
	case STATE_DELAY:

		if (m_timerbeep.isExpired()){
			m_trial_fixation->hide();
			
			// Neutral 
			if (SaccCueType == 0){
				m_neutralcue_sacc->show();
//				CEnvironment::Instance()->outputMessage("Saccade Cue: Neutral");
				N++;
				T++;
				CEnvironment::Instance()->outputMessage("Prop. Neutral: %2f", N/T);
			}
			// Left directional
			else if (SaccCueType == 7){
				m_cue_sacc->degSetAngle(360);
				m_cue_sacc->show();
//				CEnvironment::Instance()->outputMessage("Saccade Cue: Left (7)");
				L++;
				T++;
				CEnvironment::Instance()->outputMessage("Prop. Left: %2f", L/T);
			}
			// Up directional
			else if (SaccCueType == 1) {
				m_cue_sacc->degSetAngle(270);
				m_cue_sacc->show();
//				CEnvironment::Instance()->outputMessage("Saccade Cue: Up (1)");
				U++;
				T++;
				CEnvironment::Instance()->outputMessage("Prop. Up: %2f", U / T);
			}
			// Right directional
			else if (SaccCueType == 3) {
				m_cue_sacc->degSetAngle(180);
				m_cue_sacc->show();
//				CEnvironment::Instance()->outputMessage("Saccade Cue: Right (3)");
				R++;
				T++;
				CEnvironment::Instance()->outputMessage("Prop. Right: %2f", R/T);
			}
			//Down directional
			else if (SaccCueType == 5) {
				m_cue_sacc->degSetAngle(90);
				m_cue_sacc->show();
//				CEnvironment::Instance()->outputMessage("Saccade Cue: Down (5)");
				D++;
				T++;
				CEnvironment::Instance()->outputMessage("Prop. Down: %2f", D / T);
			}

			m_timerSaccCue.start(m_paramsFile->getFloat(CFG_SACCCUE_TIME));
			m_timerdelay.start(m_delayTime);
			m_state = STATE_PREPERATION;
		}

		break;

		// STATE_PREPERATION: beeps to signal the start of saccade

	case STATE_PREPERATION:

		if (m_timerSaccCue.isExpired()){
			m_cue_sacc->hide();
			m_neutralcue_sacc->hide();
			m_trial_fixation->show();
		}

		if (m_timerdelay.isExpired()){
			m_cue_sacc->hide();
			m_neutralcue_sacc->hide();
			m_trial_fixation->show();
			m_state = STATE_TARGET;
			//moveToFront(m_cue_fixation);
			//CEnvironment::Instance()->outputMessage("State Target");
			m_timertarget.start(m_targetTime);
			// time for cue presentation
			TimeTargetON = m_timerExp.getTime();
		}

		break;

		// STATE_TARGET: targets replace the place holders
	case STATE_TARGET:

		if (!m_timertarget.isExpired())
		{
			m_target1->pxSetPosition(0, TargetOffset);
			m_target2->pxSetPosition((TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
			m_target3->pxSetPosition(TargetOffset, 0);
			m_target4->pxSetPosition((TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
			m_target5->pxSetPosition(0, -TargetOffset);
			m_target6->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
			m_target7->pxSetPosition(-TargetOffset, 0);
			m_target8->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
			
			if (InhibitionTest == 1) {
				m_box_fix->pxSetPosition(0, 0);
				m_box_fix->show();

				m_target9->pxSetPosition(0, 0);
				m_target9->show();
				moveToFront(m_target9);
			}
			
			m_target1->show();
			m_target2->show();
			m_target3->show();
			m_target4->show();
			m_target5->show();
			m_target6->show();
			m_target7->show();
			m_target8->show();

			moveToFront(m_target1);
			moveToFront(m_target2);
			moveToFront(m_target3);
			moveToFront(m_target4);
			moveToFront(m_target5);
			moveToFront(m_target6);
			moveToFront(m_target7);
			moveToFront(m_target8);
		}
		else
		{
			m_target1->hide();
			m_target2->hide();
			m_target3->hide();
			m_target4->hide();
			m_target5->hide();
			m_target6->hide();
			m_target7->hide();
			m_target8->hide();
			m_target9->hide();

			m_box1->pxSetPosition(0, TargetOffset);
			m_box2->pxSetPosition((TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
			m_box3->pxSetPosition(TargetOffset, 0);
			m_box4->pxSetPosition((TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
			m_box5->pxSetPosition(0, -TargetOffset);
			m_box6->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), -(TargetOffset*(1 / sqrt(2))));
			m_box7->pxSetPosition(-TargetOffset, 0);
			m_box8->pxSetPosition(-(TargetOffset*(1 / sqrt(2))), (TargetOffset*(1 / sqrt(2))));
			m_box_fix->pxSetPosition(0,0);

			m_box1->show();
			m_box2->show();
			m_box3->show();
			m_box4->show();
			m_box5->show();
			m_box6->show();
			m_box7->show();
			m_box8->show();
			m_box_fix->show();
			m_timermask.start(m_maskTime);
			m_state = STATE_MASK;
			// time for cue presentation
			TimeMaskON = m_timerExp.getTime();
		}


		break;

		// STATE_MASK: placeholders are back as a mask before cue is presented
	case STATE_MASK:

		if (!m_timermask.isExpired())
		{

		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Cue");
			m_cue->pxSetPosition(cueX + Xlocation, cueY + Ylocation);
			//m_cue->pxSetPosition(60, 60);
			moveToFront(m_cue);
			m_cue->show();

			m_timercue.start(m_cueTime);
			m_state = STATE_CUE;
			// time for cue presentation
			TimeCueON = m_timerExp.getTime();
		}


		break;


		// STATE_CUE: present the cue
	case STATE_CUE:

		if (!m_timercue.isExpired())
		{
			moveToFront(m_cue);
			m_cue->show();
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Response");
			m_state = STATE_RESPONSE;
			hideAllObjects();
			m_cue->degSetAngle(90);
			WAIT_RESPONSE = 1;
			m_timerresponse.start(m_responseTime);
			// time for cue presentation
			TimeResponseON = m_timerExp.getTime();
		}
		break;

		// STATE_RESPONSE: wait for subject response 
	case STATE_RESPONSE:

		if (((m_timerresponse.isExpired())) || (WAIT_RESPONSE == 0))
		{
			if (WAIT_RESPONSE == 1)
				CEnvironment::Instance()->outputMessage("Response not given");
			//CEnvironment::Instance()->outputMessage("Trial ends");
			endTrial();
			saveData();
		}

		break;

	}}		
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventJoypad()
{
// activate the joypad only in the state calibration	

	if (m_state == STATE_TESTCALIBRATION) 
	{				

		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP)) // moving the cursor up
		{
			yPos = yPos + Increment; //position of the cross
		}
	
		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_DOWN)) // moving the cursor down
		{	
			yPos = yPos - Increment;
		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_RGHT)) // moving the cursor to the right
		{
			xPos = xPos + Increment;

		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_LEFT)) // moving the cursor to the left
		{
			xPos = xPos - Increment;
		
		}
		
		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_X)) // finalize the response
		{
				//CEnvironment::Instance()->outputMessage("Recalibration finalized");
				ResponseFinalize = 1; // click the left botton to finalize the response

		}
   }
	if (m_state == STATE_RESPONSE)
	{
		if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) |
			(CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) )
		{
			WAIT_RESPONSE = 0;
			TotalResponses++;
			// get the time of the response here
			ResponseTime =  m_timerExp.getTime();
		
		// right press for rightward
		if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) )// target tilted R
		{					
			CEnvironment::Instance()->outputMessage("\nSubject's Response: Right");
			Response = 1;
		}

		if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) )// target tilted L
		{
			CEnvironment::Instance()->outputMessage("\nSubject's Response: Left");
			Response = 0;
		}

		if (CuedTargetOrientation == 0) {
			CEnvironment::Instance()->outputMessage("Target Orientation: Left");
		}
		else if (CuedTargetOrientation == 1) {
			CEnvironment::Instance()->outputMessage("Target Orientation: Right");
		}

		if (CuedTargetOrientation == Response)
		{
			Correct = 1;
			if (SaccCueType == 0) {
				neutral_correct++;
			}
			TotalCorrect++;
			CEnvironment::Instance()->outputMessage("Correct\n");
		}
		else
		{
			Correct = 0;
			CEnvironment::Instance()->outputMessage("Wrong\n");
		}
		
		if (SaccCueType == 0 ) {
			CEnvironment::Instance()->outputMessage("Neutral Correct: %2f", neutral_correct / N);
		}
		CEnvironment::Instance()->outputMessage("Prop Correct: %2f", TotalCorrect/TotalResponses);
;
		}


	}
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::gotoFixation()
{

    if (!(TestCalibration == 1))
        CEnvironment::Instance()->outputMessage("Trial Number: %d\n", (TrialNumber));


    hideAllObjects();


    if (TestCalibration == 1){
        m_state = STATE_TESTCALIBRATION;
        //CEnvironment::Instance()->outputMessage("Calibration trial");
        ResponseFinalize = 0;
        m_timerCheck.start(500);
		startTrial();
    }
    else{

        m_state = STATE_FIXATION;
        gate = 1;


		// if the response is not given Correct is set at 10
		Correct = 10;
		// list with the orientations of targets at all locations
		// pick orientation randomly (45 -> rightward 135-> leftward)
		int choices[2] = { 45, 135 };
		int id = rand() % 2;
		Target1Orientation = choices[id];
		id = rand() % 2;;
		Target2Orientation = choices[id];
		id = rand() % 2;
		Target3Orientation = choices[id];
		id = rand() % 2;
		Target4Orientation = choices[id];
		id = rand() % 2;
		Target5Orientation = choices[id];
		id = rand() % 2;;
		Target6Orientation = choices[id];
		id = rand() % 2;
		Target7Orientation = choices[id];
		id = rand() % 2;
		Target8Orientation = choices[id];
		id = rand() % 2;
		Target9Orientation = choices[id];
		id = rand() % 2;
		m_target1->degSetAngle(Target1Orientation);
		m_target2->degSetAngle(Target2Orientation);
		m_target3->degSetAngle(Target3Orientation);
		m_target4->degSetAngle(Target4Orientation);
		m_target5->degSetAngle(Target5Orientation);
		m_target6->degSetAngle(Target6Orientation);
		m_target7->degSetAngle(Target7Orientation);
		m_target8->degSetAngle(Target8Orientation);
		m_target9->degSetAngle(Target9Orientation);
		
		// cue location expressed by quadrant
		int locations[9] = {1,3,5,7,9,9,9,9,9};		
		CueLocation = locations[rand() % 9];

		//CueLocation = 9;

		// Determine saccade cue location
		int arr[] = {0, 1, 3, 5, 7};
		int freq[] = {40, 15, 15, 15, 15};
		int n = sizeof(arr) / sizeof(arr[0]);
		SaccCueType = myRand(arr, freq, n);

		// define cue coordinates
		if (CueLocation == 1)
		{
			cueX = 0;
			cueY = TargetOffset + BoxSize;

			if (Target1Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 2)
		{
			m_cue->degSetAngle(225);
			cueX = (TargetOffset * (1 / sqrt(2))) + BoxSize;
			cueY = (TargetOffset * (1/sqrt(2))) + BoxSize;
			if (Target2Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 3)
		{
			m_cue->degSetAngle(0);
			cueX = TargetOffset+BoxSize;
			cueY = 0;
			if (Target3Orientation == 45)
			{
				CuedTargetOrientation = 1;


			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 4)
		{
			m_cue->degSetAngle(135);
			cueX = (TargetOffset * (1 / sqrt(2))) + BoxSize;
			cueY = -(TargetOffset * (1 / sqrt(2))) - BoxSize;
			if (Target4Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 5)
		{
			cueX = 0;
			cueY = -TargetOffset - BoxSize;
			if (Target5Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 6)
		{
			m_cue->degSetAngle(225);
			cueX = -(TargetOffset * (1 / sqrt(2))) - BoxSize;
			cueY = -(TargetOffset * (1 / sqrt(2))) - BoxSize;
			if (Target6Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 7)
		{
			m_cue->degSetAngle(0);
			cueX = -TargetOffset - BoxSize;
			cueY = 0;
			if (Target7Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 8)
		{
			m_cue->degSetAngle(135);
			cueX = -(TargetOffset * (1 / sqrt(2))) - BoxSize;
			cueY = (TargetOffset * (1 / sqrt(2))) + BoxSize;
			if (Target8Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 9)
		{
			cueX = 0;
			cueY = BoxSize;
			if (Target9Orientation == 45)
			{
				CuedTargetOrientation = 1;
			}
			else
			{
				CuedTargetOrientation = 0;
			}
		}
		CEnvironment::Instance()->outputMessage("Cued Location   | Sacc. Cue. Location");
		CEnvironment::Instance()->outputMessage("            %i               |              %i            ", CueLocation, SaccCueType);


		// start the trial
		WAIT_RESPONSE = 1;
		startTrial();
		m_timerExp.start();
		m_timer.start(1000);
		gate = 1;



	}
		
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::saveData()
{

		if (ResponseTime>0)
	    // give confrimation of response
		Beep(600,400);

		// time of the response (locked to the start of the trial)	
		storeTrialVariable("InhibitionTest", InhibitionTest);
		storeTrialVariable("TimeCueON", TimeCueON);
		storeTrialVariable("TimeFixationON", TimeFixationON); 
		storeTrialVariable("TimeTargetON", TimeTargetON);	
		storeTrialVariable("TimeMaskON", TimeMaskON);
		storeTrialVariable("TimeResponseCueON", TimeResponseON);
		storeTrialVariable("ResponseDuration", ResponseTime - TimeResponseON);
		storeTrialVariable("Correct", Correct);
		// event total time
		storeTrialVariable("FixationTime", m_fixationTime);
		storeTrialVariable("BeepDelayTime", m_beepdelay);
		storeTrialVariable("DelayTime", m_delayTime);
		storeTrialVariable("TargetTime", m_targetTime);
		storeTrialVariable("CueTime", m_cueTime);
		storeTrialVariable("MaskTime", m_maskTime);
		// save target orientation
		storeTrialVariable("Target1Orientation", Target1Orientation);
		storeTrialVariable("Target2Orientation", Target2Orientation);
		storeTrialVariable("Target3Orientation", Target3Orientation);
		storeTrialVariable("Target4Orientation", Target4Orientation);
		storeTrialVariable("Target5Orientation", Target5Orientation);
		storeTrialVariable("Target6Orientation", Target6Orientation);
		storeTrialVariable("Target7Orientation", Target7Orientation);
		storeTrialVariable("Target8Orientation", Target8Orientation);
		storeTrialVariable("Target9Orientation", Target9Orientation);
		// x location and y location of objects based on initial gaze position in px with offset added
		storeTrialVariable("XlocationPx", Xlocation);
		storeTrialVariable("YlocationPx", Ylocation);

		// dimensions targets
		storeTrialVariable("FixSize", FixSize); //px
		storeTrialVariable("BoxSize", BoxSize);
		storeTrialVariable("TSizeX", TSizeX);
		storeTrialVariable("TSizeY", TSizeY);

		// type of saccade cue 
		storeTrialVariable("SaccCueType", SaccCueType); // 0 = neutral 1 = directional R 2 = directional L


		// save cue location
		storeTrialVariable("CueLocation", CueLocation);	
		storeTrialVariable("CuedTargetOrientation", CuedTargetOrientation);

		// save target rgb value
		storeTrialVariable("TargetRGB", m_paramsFile->getFloat(CFG_RGB_VALUE));
		// save target size in px
		storeTrialVariable("TSizeXpx", TSizeX);
		storeTrialVariable("TSizeYpx", TSizeY);
	
		storeTrialVariable("Response", Response);				
		storeTrialVariable("ResponseTime", ResponseTime);
		storeTrialVariable("TargetOffsetpx", TargetOffset);//px
		
		storeTrialVariable("RefreshRate", m_paramsFile->getFloat(CFG_REFRESH_RATE));
		storeTrialVariable("Xres", m_paramsFile->getFloat(CFG_X_RES));
		storeTrialVariable("Yres", m_paramsFile->getFloat(CFG_Y_RES));
		
		
		storeTrialVariable("Subject_Name", m_paramsFile->getString(CFG_SUBJECT_NAME));
		
		// save information about the test calibration
		storeTrialVariable("TestCalibration", TestCalibration);
		storeTrialVariable("xoffset", xshift);
		storeTrialVariable("yoffset", yshift); //px

		storeTrialVariable("SaccCueTime", m_paramsFile->getFloat(CFG_SACCCUE_TIME)); // saccade cue time
		


		saveTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME));

		
		// keep track of the test calibration trials
		m_numTestCalibration++;
		
		// recalibration active at each trial (here set at 1)
		if (m_numTestCalibration == 1) 
	    {
			xPos = 0;
			yPos = 0;
			TestCalibration = 1;
			ResponseFinalize = 0;
			m_numTestCalibration = 0;
			m_timerCheck.start(100);
			m_whitecross->pxSetPosition(0,0);
			m_whitecross->show();
	    }

		
		TrialNumber++;
		CEnvironment::Instance()->outputMessage("-----------------------------------------------------");
		gotoFixation();
		
}
///////////////////////////////////////////////////////////////////////////////////
std::string ExperimentBody::int2string(int x){

	stringstream temps;
	temps << x;
	return temps.str();
}

////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
// random number generator based on relative frequency

// Utility function to find ceiling of r in arr[l..h]
int ExperimentBody::findCeil(std::vector<int> arr, int r, int l, int h)
{
	int mid;
	while (l < h)
	{
		mid = l + ((h - l) >> 1);  // Same as mid = (l+h)/2
		(r > arr[mid]) ? (l = mid + 1) : (h = mid);
	}
	return (arr[l] >= r) ? l : -1;
}
// The main function that returns a random number from arr[] according to
// distribution array defined by freq[]. n is size of arrays.
int ExperimentBody::myRand(int arr[], int freq[], int n)
{
	srand(time(NULL));
	// Create and fill prefix array
	std::vector<int> prefix(n, 0);
	int i;
	prefix[0] = freq[0];
	for (i = 1; i < n; ++i)
		prefix[i] = prefix[i - 1] + freq[i];

	// prefix[n-1] is sum of all frequencies. Generate a random number
	// with value from 1 to this sum
	int r = (rand() % prefix[n - 1]) + 1;

	// Find index of ceiling of r in prefix arrat
	int indexc = findCeil(prefix, r, 0, n - 1);
	return arr[indexc];
}