
// Template.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include <emil/emil.hpp>

const std::string CFG_SUBJECT_NAME = "Subject";
const std::string CFG_DATA_DESTINATION = "DataDestination";
const std::string CFG_FIXSIZE = "FixSize";
const std::string CFG_BOXSIZE = "BoxSize";
const std::string CFG_TSIZEX = "TSizeX";
const std::string CFG_TSIZEY = "TSizeY";
const std::string CFG_TARGET_OFFSET = "TargetOffset";
const std::string CFG_RGB_VALUE = "RgbValue";
const std::string CFG_FIXATION_TIME = "FixationTime";
const std::string CFG_CUE_TIME = "CueTime";
const std::string CFG_TARGET_TIME = "TargetTime";
const std::string CFG_RESPONSE_TIME = "ResponseTime";
const std::string CFG_MASK_TIME = "MaskTime";
const std::string CFG_SACCCUE_TIME = "SaccCueTime";

const std::string CFG_DELAY_TIME = "DelayTime";

const std::string CFG_INHIBITION_TEST = "InhibitionTest";

const std::string CFG_X_RES = "X_res";
const std::string CFG_Y_RES = "Y_res";
const std::string CFG_REFRESH_RATE = "RefreshRate";

// CTemplateApp:
// See Template.cpp for the implementation of this class
//

class CMicrosaccadeEnhancement : public CWinApp
{
public:
	CMicrosaccadeEnhancement();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	
	// Configuration file
	CCfgFile m_paramsFile;
};

extern CMicrosaccadeEnhancement theApp;
