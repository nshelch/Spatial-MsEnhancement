#ifndef _AutoCalibration_H
#define _AutoCalibration_H

#include "stdafx.h" 
#include "emil\CTimer.hpp"
#include "emil\CDSPDataStream.hpp"
#include "emil\ExCalibrator.hpp"
#include "emil\CEOS.hpp"
#include "emil\CImagePlane.hpp"
#include "MicrosaccadeEnhancement.h"

/// Automatic calibration experiment
/** This experiment exists to calibrate the mapping between voltages as reported by the 
		eyetracker and arcminutes of visual angle. This is accomplished by displaying nine points 
		in a square grid onscreen, and instructing the subject to fixate on each one in turn
		and capturing the gaze location in response to a button press when the subject feels
		he/she is in the center of the fixation cross.  The experiment determines the fixation 
		location in voltage space, and computes regressions to determine the mapping into 
		angle space. */
class AutoCalibration: public ExCalibrator
{
public:
	/// Default constructor
	/** @param pxWidth Width of the monitor in pixels
			@param pxHeight Height of the monitor in pixels
			@param RefreshRate Refresh rate of the monitor, in Hz
			@param OutputDir String of the directory name where to save the output files */
	AutoCalibration(int pxWidth, int pxHeight, int RefreshRate, std::string OutputDir = "");

	/// Standard event handlers
	void initialize();
	void finalize();
	void eventRender(unsigned int FrameCount, CEOSData* Samples);
	void eventJoypad();
	void eventKeyboard(unsigned char key, int x, int y);
	enum {
		Offset = 100,			///< Number of calibration points
	};

private:

	/// Calculate the fixation center
	/** To ensure that the true center is found, capture the gaze location upon subject's 
	button press.*/
	bool _calculateCenter(const vector<float>& DataVector, float Threshold, float& Center);

	///Calculate array of quadrant indicies
	void _calcQuadrants(int centersIndicies[]);

	/// Initialize a new calibration
	void _startNewCalibration();

	/// Load an old calibration
	void _loadOldCalibration();

	/// The current calibration state
	enum {
		STATE_QUESTION,
		STATE_BADLOAD,
		STATE_POINT,
		STATE_ERROR
	} m_state;

	/// The fixation dot
	//CImagePlane* m_planeStimulus;
	CSolidPlane* m_planeStimulus;

	/// File name of the stimulus used during the calibration
	std::string m_fileStimulus;

	/// The currently-displayed calibration point
	int m_currentPoint;
	
	/// Buffer to hold the raw eyetracker data
	std::vector<float> m_xData;
	std::vector<float> m_yData;

	/// A timer for pausing to allow subject to fixate
	CTimer m_timer;

	/// Pointer to the welcome banner
	CImagePlane* m_welcomeBanner;

	float m_paramCalThreshold; ///< Standard deviation threshold for the center-finding algorithm
	unsigned int m_paramMaxData; ///< Approximate maximum samples collected per point
	unsigned int m_paramPrepointWait; ///< Seconds to wait after displaying new point before collecting data
	unsigned int m_paramMinValidData;	///< Minimum valid data to calculate center
	unsigned int m_paramReactionDelay;	///<Offset into data for center calculation due to subject's reaction time 
	unsigned int m_paramWindow; ///<Size of data-window to use in calibration
};

#endif	// _AutoCalibration_H