#include "stdafx.h" 
#include "ManualCalibration.h"

//#include "Conversion.h"
#include "emil\CEnvironment.hpp"
#include "emil\CEnvVariables.hpp"
#include "emil\CExceptions.hpp"
#include "emil\CEOS.hpp"
#include "common\EOS_common.hpp"
#include "emil\CFontEngine.hpp"
#include "emil\CMath.hpp"
#include "emil\CStabilizer.hpp"

///////////////////////////////////////////////////////////////////////////////////
// Default constructor
ManualCalibration::ManualCalibration(int pxWidth, int pxHeight, int RefreshRate, std::string OutputDir) :
ExCalibrator(pxWidth, pxHeight, RefreshRate)
{
	setExperimentName("Monocular Linear Manual Calibrator");

	setOutputDir(OutputDir);

	// Retrieve the file name of the stimulus used to indicate 
	// the eye position (red cross)
	m_fileStimulus = CEnvVariables::Instance()->getEYERISPathFile(CFG_N_CAL_EYEPOSITIONFILE);

	// File name of the cross stimulus used as reference marker
	m_fileCross = CEnvVariables::Instance()->getEYERISPathFile(CFG_N_CAL_STIMULUSFILE);

	m_refreshRate = RefreshRate;
}

///////////////////////////////////////////////////////////////////////////////////
void ManualCalibration::initialize()
{
	// Call its ancestor
	ExCalibrator::initialize();

	m_curPoint = 0;

	// Set the initial point coordinates
	for (int i = 0; i < POINTS_NUM; i++) {
		m_xCoo[i] = static_cast<short>(((i % 3) - 1) * Offset);
		m_yCoo[i] = static_cast<short>(-((i / 3) - 1) * Offset);


	}


	for (int i = 0; i <POINTS_NUM; i++) {
		m_xA[i] = CConverter::Instance()->px2a(m_xCoo[i]);
		m_yA[i] = CConverter::Instance()->py2a(m_yCoo[i]);
	}

	// Set the eye position stimulus (red cross)
	/*m_planeStimulus = addObject(new CImagePlane(m_fileStimulus));
	m_planeStimulus->pxSetPosition(0, 0);
	m_planeStimulus->enableTrasparency(true);*/
	m_planeStimulus = addObject(new CSolidPlane(0, 0, 0));
	//m_planeStimulus = addObject(new CSolidPlane(127, 127, 127));
	m_planeStimulus->pxSetSize(10, 10);
	m_planeStimulus->pxSetPosition(0, 0);


	// Set the initial point coordinates
	/*m_crossStimulus = addObject(new CImagePlane(m_fileCross));
	m_crossStimulus->pxSetPosition(m_xCoo[m_curPoint], m_yCoo[m_curPoint]);
	m_crossStimulus->enableTrasparency(true);*/
	m_crossStimulus = addObject(new CSolidPlane(255, 255, 255));
	m_crossStimulus->pxSetSize(10, 10);
	m_crossStimulus->pxSetPosition(m_xCoo[m_curPoint], m_yCoo[m_curPoint]);

	loadCalibrationFromFile();

	// Initial values
	m_xOffset = 0;
	m_yOffset = 0;

	// Set the jog values
	float OffsetJog = CEnvVariables::Instance()->getFloat(CFG_N_CAL_OFFSETJOG);

	m_xOffsetJog = OffsetJog / m_refreshRate;
	m_yOffsetJog = OffsetJog / m_refreshRate;

	disable(CExperiment::EIS_JP_STRT);
	disable(CExperiment::EIS_PHOTOCELL);
	enable(CExperiment::EIS_STAT1);

	// Prepare the background
	COGLEngine::Instance()->setBackgroundColor(m_backgroundColor);

	// Start the calibrated trial without recording
	startTrial(false, true);

}

///////////////////////////////////////////////////////////////////////////////////
void ManualCalibration::finalize()
{
	endTrial();

	calibrateDSP();
	saveCalibrationToFile();
}

///////////////////////////////////////////////////////////////////////////////////
void ManualCalibration::eventRender(unsigned int, CEOSData* Samples)
{
	// If the controller is in digital mode, flash the screen white
	showAllObjects();

	if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_LEFT))
		m_xOffset -= m_xOffsetJog;
	else if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_RGHT))
		m_xOffset += m_xOffsetJog;

	if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP))
		m_yOffset += m_yOffsetJog;
	else if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_DOWN))
		m_yOffset -= m_yOffsetJog;


	// Draw the offset and gain onscreen so we can watch them change
	glColor3f(255, 255, 255);
	print(CFontEngine::FONTS_ARIAL_14, static_cast<float>(-m_pxWidth / 2) + 20.0f, static_cast<float>(-m_pxHeight / 2) + 70.0f,
		"X Offset: %.3f", m_xOffset);

	print(CFontEngine::FONTS_ARIAL_14, static_cast<float>(-m_pxWidth / 2) + 20.0f, static_cast<float>(-m_pxHeight / 2) + 50.0f,
		"Y Offset: %.3f", m_yOffset);


	// Now, render the stimulus plane in the new, recalibrated location
	float x;
	float y;
	if (Samples->samplesNumber > 0) {

		// Stabilize the X
		CStabilizer::Instance()->stabilize(Samples, x, y);

		// Check the limits 
		x = (fabs(x) < (m_pxWidth / 2)) ? x : CMath::sign(x) * (m_pxWidth / 2);
		y = (fabs(y) < (m_pxHeight / 2)) ? y : CMath::sign(y) * (m_pxHeight / 2);

		print(CFontEngine::FONTS_ARIAL_BLACK_10, 0.0f, -(m_pxHeight / 2.0f) + 10, "x %f y %f", x, y);

		m_planeStimulus->pxSetPosition(x, y);
		moveToFront(m_planeStimulus);
	}
	else
		// If no samples are coming from the eyetracker, don't even try to calibrate
		return;
}

///////////////////////////////////////////////////////////////////////////////////
void ManualCalibration::eventJoypad()
{

	// If SELECT is pressed then the joypad status is reinitialized
	if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_SLCT)) {

		// Store the initial values
		m_xOffset = 0;
		m_yOffset = 0;
	}

	if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_LEFT) ||
		CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_RGHT) ||
		CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_UP) ||
		CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_DOWN)) {

		m_xA[m_curPoint] = m_xA[m_curPoint] + m_xOffset;
		m_yA[m_curPoint] = m_yA[m_curPoint] + m_yOffset;




		m_xOffset = 0;
		m_yOffset = 0;

		switch (m_curPoint) {
		case 0:
			calculateMap(0, 1);
			break;
		case 1:
			calculateMap(0, 2);
			break;
		case 2:
			calculateMap(1, 2);
			break;
		case 3:
			calculateMap(0, 1);
			calculateMap(2, 3);
			break;
		case 4:
			calculateMap(0, 4);
			break;
		case 5:
			calculateMap(1, 2);
			calculateMap(3, 4);
			break;
		case 6:
			calculateMap(2, 3);
			break;
		case 7:
			calculateMap(2, 4);
			break;
		case 8:
			calculateMap(3, 4);
			break;
		}

		calibrateDSP();


	}

	if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_R1)) {

		m_curPoint++;

		if (m_curPoint == POINTS_NUM)
			m_curPoint = 0;

		m_crossStimulus->pxSetPosition(m_xCoo[m_curPoint], m_yCoo[m_curPoint]);
		m_crossStimulus->show();
		CEnvironment::Instance()->outputMessage("Coord: %i", m_xCoo[m_curPoint]);
		//m_crossStimulus->enableTrasparency(true);

		m_xOffset = 0;
		m_yOffset = 0;

		saveCalibrationToFile();
	}
	else if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_L1)) {

		m_curPoint--;

		if (m_curPoint == -1)
			m_curPoint = POINTS_NUM;

		m_crossStimulus->pxSetPosition(m_xCoo[m_curPoint], m_yCoo[m_curPoint]);
		m_crossStimulus->show();
		//m_crossStimulus->enableTrasparency(true);

		m_xOffset = 0;
		m_yOffset = 0;

		saveCalibrationToFile();
	}

	if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_STRT))	{

		saveCalibrationToFile();
		declareFinished();
	}
}