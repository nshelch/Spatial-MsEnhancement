#ifndef _ManualCalibration_H
#define _ManualCalibration_H

#include "stdafx.h" 
#include "emil\CDSPDataStream.hpp"
#include "emil\ExCalibrator.hpp"
#include "emil\CImagePlane.hpp"
//#include "MicrosaccadesFaces.h"

/// Manual calibration experiment
/** This experiment exists to further calibrate the mapping between voltages as reported by the 
	eyetracker and arcminutes of visual angle. This is accomplished by letting the subject
	manually change gain and offset of the movements of a stabilized red X on few default
	points. */
class ManualCalibration : public ExCalibrator  
{
public:
	/// Default constructor
	/** @param pxWidth Width of the monitor in pixels
			@param pxHeight Height of the monitor in pixels
			@param RefreshRate Refresh rate of the monitor, in Hz
			@param OutputDir String of the directory name where to save the output files */
	ManualCalibration(int pxWidth, int pxHeight, int RefreshRate, std::string OutputDir = "");

	/// Standard event handlers
	void initialize();
	void finalize();
	void eventRender(unsigned int FrameCount, CEOSData* Samples);
	void eventJoypad();

	enum {
		Offset = 250,			///< Number of calibration points
	};

	// Some constants
	enum {

		LH_EYE_MIN = 90,
		LH_EYE_MAX = 160,
	
		LV_EYE_MIN = 90,
		LV_EYE_MAX = 160,

		RH_EYE_MIN = 90,
		RH_EYE_MAX = 160,

		RV_EYE_MIN = 90,
		RV_EYE_MAX = 160
	};


protected:
	/// The fixation dot
	//CImagePlane* m_planeStimulus;
	CSolidPlane* m_planeStimulus;

	///The fixation cross
	//CImagePlane* m_crossStimulus;
	CSolidPlane* m_crossStimulus;
	
	/// File name of the stimulus used during the calibration
	std::string m_fileStimulus;

	/// File name of the cross stimulus used as reference marker
	std::string m_fileCross;

	int m_refreshRate;

	int m_curPoint;

	float m_xOffset;
	float m_yOffset;

	float m_xOffsetJog;
	float m_yOffsetJog;
};

#endif	// _ManualCalibration_H
