
#define _EXPERIMENTBODY_H

#include "emil/emil.hpp"
#include <fstream>


/*#pragma once

#include "emil/EMIL.hpp"
#include <queue>
#include <io.h>*/

using namespace std;

class ExperimentBody: public CExperiment
{
public:
	ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params);

	/// Standard event handlers
	void initialize();
	void finalize();

	void eventRender(unsigned int FrameCount, CEOSData* Samples);
	//void eventKeyboard(unsigned char key, int x, int y);
	void eventJoypad();

private:

	int findCeil(std::vector<int> arr, int r, int l, int h);
	int myRand(int arr[], int freq[], int n);
	std::string ExperimentBody::int2string(int x);
	int ExperimentBody::string2int(std::string x);
	int temp_int;

	// Current experiment state
	enum STATE {
		STATE_LOADING,
		STATE_TESTCALIBRATION,
		STATE_FIXATION,
		STATE_DELAY,
		STATE_PREPERATION,
		STATE_CUE,
		STATE_TARGET,
		STATE_MASK,
		STATE_RESPONSE,
	};

	STATE m_state;

	void gotoFixation();
    void saveData();

	// Configuration file
	CCfgFile* m_paramsFile;

	CSolidPlane* m_target1;
	CSolidPlane* m_target2;
	CSolidPlane* m_target3;
	CSolidPlane* m_target4;
	CSolidPlane* m_target5;
	CSolidPlane* m_target6;
	CSolidPlane* m_target7;
	CSolidPlane* m_target8;
	CSolidPlane* m_target9;
	CImagePlane* m_cue;
	CImagePlane* m_cue_sacc;
	CImagePlane* m_neutralcue_sacc;;
	CSolidPlane* m_box1;
	CSolidPlane* m_box2;
	CSolidPlane* m_box3;
	CSolidPlane* m_box4;
	CSolidPlane* m_box5;
	CSolidPlane* m_box6;
	CSolidPlane* m_box7;
	CSolidPlane* m_box8;
	CSolidPlane* m_box_fix;
	CImagePlane* m_mask1;
	CImagePlane* m_mask2;
	CImagePlane* m_mask3;
	CImagePlane* m_mask4;
	CImagePlane* m_mask5;
	CImagePlane* m_mask6;
	CImagePlane* m_mask7;
	CImagePlane* m_mask8;
	CImagePlane* m_mask_fix;

	// Stimuli for the test calibration
	CSolidPlane* m_redcross;
	CSolidPlane* m_whitecross;
	CSolidPlane* m_fixation;
	CSolidPlane* m_trial_fixation;
	CSolidPlane* m_cue_fixation;

	float FixSize;
	float BoxSize;
	int TSizeX;
	int TSizeY;
	int SaccCueType;

	// timers
	CTimer m_timer;
	CTimer m_timerCheck;
	CTimer m_timerExp;
	CTimer m_timerfixation;
	CTimer m_timerresponse; 
	CTimer m_timertarget;
	CTimer m_timercue;
	CTimer m_timermask;
	CTimer m_timerSaccCue;

	CTimer m_timerdelay;
	CTimer m_timerbeep;
	
	float TargetOffset;	
	float ResponseTime;
	float m_targetTime;
	float m_fixationTime;
	float m_cueTime;
	float m_maskTime;
	float m_responseTime;
	float TimeResponseON;
	float TimeMaskON;

	float m_delayTime;
	int m_beepdelay;

	int Increment;
	int m_numTestCalibration;
	int ResponseFinalize;
	int TestCalibration;
	int WAIT_RESPONSE;
	int gate;
	int TrialNumber;
	int debug;
	int Correct;
	int Response;
	int InhibitionTest;
	
	float TimeTargetON;
	float TimeCueON;
	float TimeFixationON;
	float xPos;
	float yPos;
	float xshift;
	float yshift;
	float N;
	float U;
	float D;
	float T;
	float L;
	float R;
	float cong_total;

	int InhibitionCounter;
	int neutral_correct;
	int cong_correct;
	int CueLocation;
	int CuedTargetOrientation;
	int Target1Orientation;
	int Target2Orientation;
	int Target3Orientation;
	int Target4Orientation;
	int Target5Orientation;
	int Target6Orientation;
	int Target7Orientation;
	int Target8Orientation;
	int Target9Orientation;
	float Xlocation;
	float Ylocation;
	float TotalCorrect;
	float TotalResponses;

	float cueX;
	float cueY;

};
