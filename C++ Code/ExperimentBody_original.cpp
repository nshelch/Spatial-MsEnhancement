#include "stdafx.h"
#include "ExperimentBody.h"
#include "MicrosaccadeEnhancement.h"
#include <math.h>

const float PI = 3.1415926353f;

// 11/15/16
//Events sequence:
//1. recalibration
//2. presentation of place holders at four locations (500 ms) 20' away from the center of gaze
//3. presentation of the target stimuli (50 ms)
//4. place holders are put back in place (100 ms)
//5. one of the four stimuli locations is cued (100 ms)
//5. subject's response (orientation of the target L or R?)


///////////////////////////////////////////////////////////////////////////////////
ExperimentBody::ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params) :
	CExperiment(pxWidth, pxHeight, RefreshRate), 

	m_paramsFile(Params)
{
	setExperimentName("Microsaccades enhancement");

	CMath::srand(time(NULL));
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::initialize()
{		
	CStabilizer::Instance()->enableSlowStabilization(true);
	
	// locations of boxes and targets
	TargetOffset = m_paramsFile->getInteger(CFG_TARGET_OFFSET);
		
	// define central fixation marker
	m_fixation = addObject(new CSolidPlane(255,255,255));
    m_fixation->pxSetSize(2,2);
	m_fixation->pxSetPosition(0,0);
	m_fixation->hide();
	
	// define the cue
	m_cue = addObject(new CImagePlane("images/cue.tga"));
	m_cue->enableTrasparency(true);
	m_cue->pxSetPosition(0,0);
	m_cue->hide();
	
	// define the place holders
	m_box1 = addObject(new CImagePlane("images/boxsmall.tga"));    
	m_box2 = addObject(new CImagePlane("images/boxsmall.tga"));
	m_box3 = addObject(new CImagePlane("images/boxsmall.tga"));
    m_box4 = addObject(new CImagePlane("images/boxsmall.tga"));	
	
	m_box1->pxSetPosition(TargetOffset, TargetOffset);
	m_box2->pxSetPosition(TargetOffset, -TargetOffset);
	m_box3->pxSetPosition(-TargetOffset,-TargetOffset);
	m_box4->pxSetPosition(-TargetOffset, TargetOffset);
	
	m_box1->hide();
	m_box2->hide();
	m_box3->hide();
	m_box4->hide();
	
	// target is a small bar
	m_target1 = addObject(new CImagePlane("images/target.tga"));
	m_target1->enableTrasparency(true);
	m_target1->pxSetPosition(TargetOffset, TargetOffset);
	m_target1->hide();
	m_target2 = addObject(new CImagePlane("images/target.tga"));
	m_target2->enableTrasparency(true);
	m_target2->pxSetPosition(TargetOffset, -TargetOffset);
	m_target2->hide();
	m_target3 = addObject(new CImagePlane("images/target.tga"));
	m_target3->enableTrasparency(true);
	m_target3->pxSetPosition(-TargetOffset,-TargetOffset);
	m_target3->hide();
	m_target4 = addObject(new CImagePlane("images/target.tga"));
	m_target4->enableTrasparency(true);
	m_target4->pxSetPosition(-TargetOffset, TargetOffset);
	m_target4->hide();
		
	// performance keepers
	TotalCorrect = 0;
	TotalResponses = 0;

	// set the timers
	m_fixationTime = m_paramsFile->getFloat(CFG_FIXATION_TIME);
	m_targetTime = m_paramsFile->getFloat(CFG_TARGET_TIME);
	m_cueTime = m_paramsFile->getFloat(CFG_CUE_TIME);
	m_responseTime = m_paramsFile->getFloat(CFG_RESPONSE_TIME);
	m_maskTime = m_paramsFile->getFloat(CFG_MASK_TIME);
	
	// set the pixel increment for the test calibration procedure during the exp
	// NOTE: smaller increments (in px) more precise is the recalibration but it will also take more time
	Increment = 1;
	ResponseFinalize = 0;
	xshift = 0;
	yshift = 0;
	xPos = 0;
	yPos = 0;
	TrialNumber = 1;
	m_numTestCalibration = 0;
	// set TestCalibration = 1 so that the experiment will start with a recalibration trial
	TestCalibration = 1;
	
	// boxes for the recalibration trials	
	m_whitecross = addObject(new CSolidPlane(255, 255, 255));
	m_whitecross->pxSetSize(10,10);
	m_redcross = addObject(new CSolidPlane(0, 0, 0));
	m_redcross->pxSetSize(10,10);


	disable(CExperiment::EIS_PHOTOCELL);
	disable(CExperiment::EIS_NOTRACK_ICON);
	disable(CExperiment::EIS_STAT1);
	
	debug = m_paramsFile->getInteger(CFG_DEBUG);
	
	hideAllObjects();
	m_state = STATE_LOADING;
	m_timer.start(1000);

	WAIT_RESPONSE = 1;
	
	/* Seed the random-number generator with current time so that
    * the numbers will be different every time we run.*/
	
    srand( (unsigned)time( NULL ) );


}
///////////////////////////////////////////////////////////////////////////////////
// write the progress file
void ExperimentBody::finalize()
{
		declareFinished();


}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventRender(unsigned int FrameCount, CEOSData* Samples)
{
	float x;
	float y;

	switch (m_state) {
	case STATE_LOADING:


		COGLEngine::Instance()->clearScreen();
		glColor3d(255, 255, 255);

		// Set the background color for the experiment
		COGLEngine::Instance()->setBackgroundColor(127,127,127);

		// Copy the parameter file into the subject directory
		if (m_timer.isExpired())
		{
			char LocalDate[1024];
			time_t t = time(NULL);
			strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t));

			ostringstream DestinationFileName;
			DestinationFileName << m_paramsFile->getDirectory(CFG_DATA_DESTINATION) <<
				m_paramsFile->getString(CFG_SUBJECT_NAME) << "/" << m_paramsFile->getString(CFG_SUBJECT_NAME) <<
				"-" << LocalDate << "-params.cfg";;
			gotoFixation();
		}

		break;

	case STATE_TESTCALIBRATION:

		m_box1->hide();
		m_box2->hide();
		m_box3->hide();
		m_box4->hide();

		if (!m_timerCheck.isExpired())
		{
			CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Test Calibration, ResponseFinalize: %.0i", ResponseFinalize);
			if (!(ResponseFinalize == 1))
			{
				CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
				moveToFront(m_redcross);
				m_redcross->pxSetPosition(x + xshift + xPos, y + yshift + yPos);
				//m_redcross->pxSetPosition(x,y);
				m_redcross->show();
				m_whitecross->pxSetPosition(0, 0);
				m_whitecross->show();
			}
			else
			{
				//CEnvironment::Instance()->outputMessage("State Test Calibration");
				TestCalibration = 0;

				xshift = xPos + xshift;
				yshift = yPos + yshift;
				//CEnvironment::Instance()->outputMessage("State Test Calibration, xshift: %.2f", xshift);
				//CEnvironment::Instance()->outputMessage("State Test Calibration, yshift: %.2f", yshift);
				//CEnvironment::Instance()->outputMessage("----------------------------------------------------------------");


				m_redcross->hide();
				m_whitecross->hide();

				endTrial();
				gotoFixation();

			}

		}
		break;

		//  STATE_FIXATION: the place holders are presented together with the central fixation point.
	case STATE_FIXATION:

		if (gate == 1)
		{
			m_box1->show();
			m_box2->show();
			m_box3->show();
			m_box4->show();
			//m_fixation->show();

			// time for fixation ON
			TimeFixationON = m_timerExp.getTime();
			//CEnvironment::Instance()->outputMessage("State Fixation");
			// fixation should stay on for one sec or as far as the gaze is stabilized
			m_timerfixation.start(m_fixationTime);
			gate = 0;
		}

		if (m_timerfixation.isExpired())
		{
			m_state = STATE_TARGET;
			//CEnvironment::Instance()->outputMessage("State Target");
			m_timertarget.start(m_targetTime);
			m_box1->hide();
			m_box2->hide();
			m_box3->hide();
			m_box4->hide();
			// time for cue presentation
			TimeTargetON = m_timerExp.getTime();
		}

		break;

		// STATE_TARGET: targets replace the place holders
	case STATE_TARGET:

		if (!m_timertarget.isExpired())
		{
			m_target1->show();
			m_target2->show();
			m_target3->show();
			m_target4->show();
		}
		else
		{
			m_target1->hide();
			m_target2->hide();
			m_target3->hide();
			m_target4->hide();

			m_box1->show();
			m_box2->show();
			m_box3->show();
			m_box4->show();

			m_timermask.start(m_maskTime);
			m_state = STATE_MASK;
			// time for cue presentation
			TimeMaskON = m_timerExp.getTime();
		}


		break;

		// STATE_MASK: placeholders are back as a mask before cue is presented
	case STATE_MASK:

		if (!m_timermask.isExpired())
		{
			m_box1->show();
			m_box2->show();
			m_box3->show();
			m_box4->show();
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Cue");
			m_cue->pxSetPosition(cueX, cueY);
			moveToFront(m_cue);
			m_cue->show();

			m_timercue.start(m_cueTime);
			m_state = STATE_CUE;
			// time for cue presentation
			TimeCueON = m_timerExp.getTime();
		}


		break;


		// STATE_CUE: present the cue
	case STATE_CUE:

		if (!m_timercue.isExpired())
		{
			moveToFront(m_cue);
			m_cue->show();
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Response");
			m_state = STATE_RESPONSE;
			hideAllObjects();
			WAIT_RESPONSE = 1;
			m_timerresponse.start(m_responseTime);
			// time for cue presentation
			TimeResponseON = m_timerExp.getTime();
		}
		break;

		// STATE_RESPONSE: wait for subject response 
	case STATE_RESPONSE:

		if (((m_timerresponse.isExpired())) || (WAIT_RESPONSE == 0))
		{
			if (WAIT_RESPONSE == 1)
				CEnvironment::Instance()->outputMessage("Response not given");
			//CEnvironment::Instance()->outputMessage("Trial ends");
			endTrial();
			saveData();
		}

		break;

	}
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventJoypad()
{
	// activate the joypad only in the state calibration	

			if (m_state == STATE_TESTCALIBRATION) 
			{				

				if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP)) // moving the cursor up
				{
					yPos = yPos + Increment; //position of the cross
				}
			
				else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_DOWN)) // moving the cursor down
				{	
					yPos = yPos - Increment;
				}
		
				else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_RGHT)) // moving the cursor to the right
				{
					xPos = xPos + Increment;

				}

				else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_LEFT)) // moving the cursor to the left
				{
					xPos = xPos - Increment;
				
				}
				
				if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_X)) // finalize the response
				{
					    //CEnvironment::Instance()->outputMessage("Recalibration finalized");
						ResponseFinalize = 1; // click the left botton to finalize the response

				}
		   }
			if (m_state == STATE_RESPONSE)
			{
				if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) |
					(CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) )
				{
					WAIT_RESPONSE = 0;
					TotalResponses++;
					// get the time of the response here
					ResponseTime =  m_timerExp.getTime();
				
				// right press for rightward
				if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) )// target tilted R
				{					
					CEnvironment::Instance()->outputMessage("Subject's response: R");
					Response = 1;
				}

				if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) )// target tilted L
				{
					CEnvironment::Instance()->outputMessage("Subject's response: L");
					Response = 0;
				}
				if (CuedTargetOrientation == Response)
				{
					Correct = 1;
					TotalCorrect++;
					CEnvironment::Instance()->outputMessage("Correct");
				}
				else
				{
					Correct = 0;
					CEnvironment::Instance()->outputMessage("Wrong");
				}
				   
				CEnvironment::Instance()->outputMessage("Prop correct: %2f", TotalCorrect/TotalResponses);
				}


			}
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::gotoFixation()
{

	if (!(TestCalibration == 1))
		CEnvironment::Instance()->outputMessage("Trial Number: %d", (TrialNumber));


	hideAllObjects();


	if (TestCalibration == 1)
	{
		m_state = STATE_TESTCALIBRATION;
		//CEnvironment::Instance()->outputMessage("Calibration trial");
		ResponseFinalize = 0;
		startTrial();
		m_timerCheck.start(500);
	}
	else
	{

		m_state = STATE_FIXATION;
		gate = 1;

		// if the response is not given Correct is set at 10
		Correct = 10;
		// list with the orientations of targets at all locations
		// pick orientation randomly (45 -> rightward 135-> leftward)
		int choices[2] = { 45, 135 };
		int id = rand() % 2;
		Target1Orientation = choices[id];
		id = rand() % 2;;
		Target2Orientation = choices[id];
		id = rand() % 2;
		Target3Orientation = choices[id];
		id = rand() % 2;
		Target4Orientation = choices[id];
		id = rand() % 2;
		m_target1->degSetAngle(Target1Orientation);
		m_target2->degSetAngle(Target2Orientation);
		m_target3->degSetAngle(Target3Orientation);
		m_target4->degSetAngle(Target4Orientation);

		// cue location expressed by quadrant
		int locations[4] = { 1, 2, 3, 4 };
		CueLocation = locations[rand() % 4];
		//CEnvironment::Instance()->outputMessage("%i", CueLocation);
		// define cue coordinates
		if (CueLocation == 1)
		{
			cueX = TargetOffset;
			cueY = TargetOffset;
			CEnvironment::Instance()->outputMessage("Cue on quad 1");
			if (Target1Orientation == 45)
			{
				CEnvironment::Instance()->outputMessage("cued target orientation R");
				CuedTargetOrientation = 1;
			}
			else
			{
				CEnvironment::Instance()->outputMessage("cued target orientation L");
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 2)
		{
			cueX = TargetOffset;
			cueY = -TargetOffset;
			CEnvironment::Instance()->outputMessage("Cue on quad 2");
			if (Target2Orientation == 45)
			{
				CEnvironment::Instance()->outputMessage("cued target orientation R");
				CuedTargetOrientation = 1;
			}
			else
			{
				CEnvironment::Instance()->outputMessage("cued target orientation L");
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 3)
		{
			cueX = -TargetOffset;
			cueY = -TargetOffset;
			CEnvironment::Instance()->outputMessage("Cue on quad 3");
			if (Target3Orientation == 45)
			{
				CEnvironment::Instance()->outputMessage("cued target orientation R");
				CuedTargetOrientation = 1;
			}
			else
			{
				CEnvironment::Instance()->outputMessage("cued target orientation L");
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 4)
		{
			cueX = -TargetOffset;
			cueY = TargetOffset;
			CEnvironment::Instance()->outputMessage("Cue on quad 4");
			if (Target4Orientation == 45)
			{
				CEnvironment::Instance()->outputMessage("cued target orientation R");
				CuedTargetOrientation = 1;
			}
			else
			{
				CEnvironment::Instance()->outputMessage("cued target orientation L");
				CuedTargetOrientation = 0;
			}

		}

		// start the trial
		WAIT_RESPONSE = 1;
		startTrial();
		m_timerExp.start();
		m_timer.start(1000);
		gate = 1;



	}
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::saveData()
{

		if (ResponseTime>0)
	    // give confrimation of response
		Beep(600,400);

		// time of the response (locked to the start of the trial)		
		storeTrialVariable("TimeCueON", TimeCueON);
		storeTrialVariable("TimeFixationON", TimeFixationON); 
		storeTrialVariable("TimeTargetON", TimeTargetON);	
		storeTrialVariable("TimeMaskON", TimeMaskON);
		storeTrialVariable("Correct", Correct);
		// event total time
		storeTrialVariable("FixationTime", m_fixationTime);
		storeTrialVariable("TargetTime", m_targetTime);
		storeTrialVariable("CueTime", m_cueTime);
		storeTrialVariable("MaskTime", m_maskTime);
		// save target orientation
		storeTrialVariable("Target1Orientation", Target1Orientation);
		storeTrialVariable("Target2Orientation", Target2Orientation);
		storeTrialVariable("Target3Orientation", Target3Orientation);
		storeTrialVariable("Target4Orientation", Target4Orientation);
		// save cue location
		storeTrialVariable("CueLocation", CueLocation);		
	
		storeTrialVariable("Response", Response);				
		storeTrialVariable("ResponseTime", ResponseTime);
		storeTrialVariable("TargetOffset", TargetOffset);//px
		
		storeTrialVariable("RefreshRate", m_paramsFile->getFloat(CFG_REFRESH_RATE));
		storeTrialVariable("Xres", m_paramsFile->getFloat(CFG_X_RES));
		storeTrialVariable("Yres", m_paramsFile->getFloat(CFG_Y_RES));
		
		
		storeTrialVariable("Subject_Name", m_paramsFile->getString(CFG_SUBJECT_NAME));
		
		// save information about the test calibration
		storeTrialVariable("TestCalibration", TestCalibration);
		storeTrialVariable("xoffset", xshift);
		storeTrialVariable("yoffset", yshift); //px

		storeTrialVariable("debug", debug); //px

		saveTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME));

		
		// keep track of the test calibration trials
		m_numTestCalibration++;
		
		// recalibration active at each trial
		if (m_numTestCalibration == 1) 
	    {
			xPos = 0;
			yPos = 0;
			TestCalibration = 1;
			ResponseFinalize = 0;
			m_numTestCalibration = 0;
			m_timerCheck.start(100);
			m_whitecross->pxSetPosition(0,0);
			m_whitecross->show();
	    }

		
		TrialNumber++;
		CEnvironment::Instance()->outputMessage("-----------------------------------------------------");
		gotoFixation();

}

