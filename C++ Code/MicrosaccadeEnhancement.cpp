
// Template.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include <emil-console\CWinEMIL.hpp>
#include <emil-console\CEMILConsole.hpp>
#include "MicrosaccadeEnhancement.h"
#include "myManualCalibrator2.h"
#include "myAutoCalibration.h"
#include "ExperimentBody.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTemplateApp

BEGIN_MESSAGE_MAP(CMicrosaccadeEnhancement, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTemplateApp construction

CMicrosaccadeEnhancement::CMicrosaccadeEnhancement()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// Initialize the manager for the configuration parameters
	m_paramsFile.addVariable(CFG_SUBJECT_NAME, std::string(""));
	m_paramsFile.addVariable(CFG_DATA_DESTINATION, std::string(""));
	m_paramsFile.addVariable(CFG_TARGET_OFFSET, 5.0f);
	m_paramsFile.addVariable(CFG_RGB_VALUE, 5.0f);
	m_paramsFile.addVariable(CFG_FIXATION_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_MASK_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_CUE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_TARGET_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_RESPONSE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_FIXSIZE, 5.0f);
	m_paramsFile.addVariable(CFG_BOXSIZE, 5.0f);
	m_paramsFile.addVariable(CFG_BOXSIZE, 5.0f);
	m_paramsFile.addVariable(CFG_TSIZEX, 5.0f);
	m_paramsFile.addVariable(CFG_TSIZEY, 5.0f);
	m_paramsFile.addVariable(CFG_SACCCUE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_DELAY_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_X_RES, 1);
	m_paramsFile.addVariable(CFG_Y_RES, 1);
	m_paramsFile.addVariable(CFG_REFRESH_RATE, 1);
	m_paramsFile.addVariable(CFG_INHIBITION_TEST, 1);
}


// The one and only CMicrosaccadeEnhancement object
CMicrosaccadeEnhancement theApp;

// CMicrosaccadeEnhancement initialization

BOOL CMicrosaccadeEnhancement::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	CWinApp::InitInstance();
	AfxEnableControlContainer();
	CShellManager *pShellManager = new CShellManager;
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
	SetRegistryKey(_T("EyeRIS @ APLab"));

	// EyeRIS ------------------------------------------------------------------
	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	CWinEMIL::Instance()->initialize("./MonitorConfiguration/emil-library-monocular-singleROG.cfg");
	// Put your code here ======================================================
	
	
	// Load the specified configuration file
	m_paramsFile.loadFile("Params.cfg");

	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	//CWinEMIL::Instance()->initialize("C:/EyeRIS/system/config/emil-library.cfg");

	std::string DestinationDir = m_paramsFile.getDirectory(CFG_DATA_DESTINATION) +
		m_paramsFile.getString(CFG_SUBJECT_NAME);


	int HResolution = m_paramsFile.getInteger(CFG_X_RES);
	int VResolution = m_paramsFile.getInteger(CFG_Y_RES);
	int RefreshRate = m_paramsFile.getInteger(CFG_REFRESH_RATE);
	//int post_time = m_paramsFile.getInteger(CFG_POST_CUE_TIME);

	// to set the initial target window
	ExTarget* target = new ExTarget(HResolution, VResolution, RefreshRate);
	target->setBackgroundColor(127, 127, 127);
	CWinEMIL::Instance()->addExperiment(target);

	//CWinEMIL::Instance()->addExperiment(new AutoCalibration(HResolution, VResolution, RefreshRate, DestinationDir + "/calibration"));
	myAutoCalibration* autoCalibrator = new myAutoCalibration(HResolution, VResolution, RefreshRate, false);
	autoCalibrator->setOutputDir(DestinationDir + "/calibration");
	autoCalibrator->setBackgroundColor(127, 127, 127);
	autoCalibrator->setPointOffset(90, 90);
	CWinEMIL::Instance()->addExperiment(autoCalibrator);

	//CWinEMIL::Instance()->addExperiment(new AutoCalibration(HResolution, VResolution, RefreshRate, DestinationDir + "/calibration"));
	myManualCalibrator2* manualCalibrator = new myManualCalibrator2(HResolution, VResolution, RefreshRate, EOS_EYE_1, false);
	manualCalibrator->setOutputDir(DestinationDir + "/calibration");
	manualCalibrator->setBackgroundColor(127, 127, 127);
	manualCalibrator->setPointOffset(90, 90);
	CWinEMIL::Instance()->addExperiment(manualCalibrator);

	CWinEMIL::Instance()->addExperiment(new ExperimentBody(HResolution, VResolution, RefreshRate, &m_paramsFile));
	
	
	// =========================================================================
	//CTemplateDlg dlg;
	//CEMILConsole dlg("C:/EyeRIS/system/config/emil-console-monocular-singleROG.cfg");
	CEMILConsole dlg("./MonitorConfiguration/emil-console-monocular-singleROG.cfg");
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();

	// Destroy the the library
	CWinEMIL::Destroy();
	// EyeRIS ------------------------------------------------------------------
	
	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

