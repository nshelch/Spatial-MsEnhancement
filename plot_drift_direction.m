function [radialDrift] = plot_drift_direction(driftFix, driftNeutral)

theta = linspace(0, 2*pi, 9); % angle (theta) vector
driftFix = cat(2, driftFix{:}); % direction vector (rho)
driftHist = hist(driftFix, theta);
driftHist(1) = driftHist(1) + driftHist(end); % combine 0 and 2*pi
driftHist = driftHist(1:end-1);% combine 0 and 2*pi
driftHist = driftHist / sum(driftHist); % normalizes the histogram
radialDrift.fix_avg = sum(exp(1i*driftFix)); % radial mean
radialDrift.fix_sem = [angle(radialDrift.fix_avg), sqrt(-2*log(abs(radialDrift.fix_avg)/length(driftFix)))]; % radial std
radialDrift.fix_hist = driftHist;

driftNeutral = cat(2, driftNeutral{:}); % direction vector (rho)
driftHist = hist(driftNeutral, theta);
driftHist(1) = driftHist(1) + driftHist(end); % combine 0 and 2*pi
driftHist = driftHist(1:end-1);% combine 0 and 2*pi
driftHist = driftHist / sum(driftHist); % normalizes the histogram
radialDrift.neutral_avg = sum(exp(1i*driftNeutral)); % radial mean
radialDrift.neutral_sem = [angle(radialDrift.neutral_avg), sqrt(-2*log(abs(radialDrift.neutral_avg)/length(driftNeutral)))]; % radial std
radialDrift.neutral_hist = driftHist;


%%% Use for across subjects
% lo = comData.fixation.saccades.directionHist(1, :)...
%     - comData.fixation.saccades.directionHist(2, :);
% hi = comData.fixation.saccades.directionHist(1, :)...
%     + comData.fixation.saccades.directionHist(2, :);
% [xlo1, ylo1] = pol2cart(paramsPP.saccades.direction, [lo, lo(1)]);
% [xhi1, yhi1] = pol2cart(paramsPP.saccades.direction, [hi, hi(1)]);

plr1 = polar(theta, ...
    [radialDrift.fix_hist(1, :),...
    radialDrift.fix_hist(1, 1)]);
p1Color = rgb('DarkGreen');
plr1.Color = p1Color;
plr1.LineWidth = 2;
hold all;

plr2 = polar(theta, ...
    [radialDrift.neutral_hist(1, :),...
    radialDrift.neutral_hist(1, 1)],'k');
plr2.LineWidth = 2;

legend([plr1, plr2], {'Fixation', 'Neutral'},'Location','none','position',[0.525 0.5 0 0])
% hold all;
% % hf1 = nan(length(xlo1)-1, 1);
% hf2 = nan(length(xlo) - 1, 1);
% for jj = 1:(length(xlo1)-1)
%     hf1(jj) = fill([xlo1(jj:jj+1) xhi1(jj+1:-1:jj)], [ylo1(jj:jj+1) yhi1(jj+1:-1:jj)],...
%         'k');
%     hf2(jj) = fill([xlo(jj:jj+1) xhi(jj+1:-1:jj)], [ylo(jj:jj+1) yhi(jj+1:-1:jj)],...
%         'b');
% end
% set([hf1, hf2], 'FaceAlpha', .3, 'EdgeColor', 'none');
% set(hf1, 'FaceColor', [.7 .7 .7]);
% set(plr2, 'Color', [.7 .7 .7]);
% set([plr1, plr2], 'LineWidth', 2);
% xlim([-.3450, .3450]); ylim([-.3450, .3450]);
% standardPlot;

end
