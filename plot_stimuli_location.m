function output = plot_stimuli_location(color, boxSize, targetOffset)

% boxSize: diameter of the box (in arcmin)
% targetOffset: distance from fixation point to center of box (in arcmin)
% xx, yy: eye positions (in arcmin)

dist_from_center = boxSize/2;
% diagonal distance from target
diag_dist = targetOffset*sqrt(2)/2;

% center location for each box
y_target_location = [0, targetOffset, diag_dist, 0, -diag_dist, -targetOffset, -diag_dist, 0, diag_dist];
x_target_location = [0, 0, diag_dist, targetOffset, diag_dist, 0, -diag_dist, -targetOffset, -diag_dist];
hold on;

if color
    set(gca, 'color', [127/255, 127/255, 127/255])
    for ii = 1:length(x_target_location)
        x_corners = [x_target_location(ii) - dist_from_center, x_target_location(ii) + dist_from_center, x_target_location(ii) + dist_from_center, x_target_location(ii) - dist_from_center];
        y_corners = [y_target_location(ii) - dist_from_center, y_target_location(ii) - dist_from_center, y_target_location(ii) + dist_from_center, y_target_location(ii) + dist_from_center];
        fill(x_corners, y_corners, [150/255, 150/255, 150/255])
    end
    axis([-40 40 -40 40])
else
    for ii = 1:length(x_target_location)
        plot([x_target_location(ii) - dist_from_center, x_target_location(ii) + dist_from_center], [y_target_location(ii) + dist_from_center, y_target_location(ii) + dist_from_center], 'k-')
        plot([x_target_location(ii) - dist_from_center, x_target_location(ii) + dist_from_center], [y_target_location(ii) - dist_from_center, y_target_location(ii) - dist_from_center], 'k-')
        plot([x_target_location(ii) + dist_from_center, x_target_location(ii) + dist_from_center], [y_target_location(ii) - dist_from_center, y_target_location(ii) + dist_from_center], 'k-')
        plot([x_target_location(ii) - dist_from_center, x_target_location(ii) - dist_from_center], [y_target_location(ii) - dist_from_center, y_target_location(ii) + dist_from_center], 'k-')
    end
    axis([-30 30 -30 30])
end

axis square
box off

output = gcf;
end
