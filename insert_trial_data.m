function [dataStruct, counter, heatmap] = insert_trial_data(dataStruct, params, counter, heatmap, splitLocation, vt, ii, msId, xx_target, yy_target)

if params.DDPI
    msStart = vt{ii}.microsaccades.startSample;
    msEnd = msStart + vt{ii}.microsaccades.durationSample;
else
    msStart = vt{ii}.microsaccades.start;
    msEnd = msStart + vt{ii}.microsaccades.duration;
end

msAmp = vt{ii}.microsaccades.amplitude;
saccCueOn = round(vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime);
targetOn = vt{ii}.TimeTargetON;
targetOff = round(vt{ii}.TimeTargetON + vt{ii}.TargetTime);
targetOffset = double(vt{ii}.TargetOffsetpx);
respCueLoc = vt{ii}.CueLocation;
saccCueLoc = vt{ii}.SaccCueType;
xOffset = vt{ii}.xoffset * vt{ii}.pxAngle;
yOffset = vt{ii}.yoffset * vt{ii}.pxAngle;
counter.valid_trials = counter.valid_trials + 1;
dataStruct.valid_trial_id(counter.valid_trials) = ii;
distFromTarget = target_classification(xx_target, yy_target, respCueLoc, targetOffset, vt{ii}.pxAngle);

heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).xx = [heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).xx, xx_target];
heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).yy = [heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).yy, yy_target];


if isempty(msId) % no ms trials
    [trialType, counter] = trial_type_classification(0, splitLocation, NaN, NaN, respCueLoc, saccCueLoc, counter);
    trialCount = counter.(trialType);
else
    
    dataStruct.ms_dir_vec_x(ii) = vt{ii}.x.position(msEnd(msId)) - vt{ii}.x.position(msStart(msId));
    dataStruct.ms_dir_vec_y(ii) = vt{ii}.y.position(msEnd(msId)) - vt{ii}.y.position(msStart(msId));
    [trialType, counter] = trial_type_classification(1, splitLocation, dataStruct.ms_dir_vec_x(ii), dataStruct.ms_dir_vec_y(ii), respCueLoc, saccCueLoc, counter);
    trialCount = counter.(trialType);
    counter.ms_trials = counter.ms_trials + 1;
    dataStruct.ms_end_xpos(ii) = vt{ii}.x.position(msEnd(msId))+ xOffset;
    dataStruct.ms_end_ypos(ii) = vt{ii}.y.position(msEnd(msId))+ yOffset;
    dataStruct.landing_error(ii) = target_classification(dataStruct.ms_end_xpos(ii), dataStruct.ms_end_ypos(ii), respCueLoc, targetOffset, vt{ii}.pxAngle);
    dataStruct.(trialType).amp(trialCount) = msAmp(msId);
    if params.DDPI
        dataStruct.(trialType).ms_latency(trialCount) = vt{ii}.microsaccades.start(msId) - targetOn;
    else
        dataStruct.(trialType).ms_latency(trialCount) = msStart(msId) - targetOn;
    end
    dataStruct.(trialType).landing_error(trialCount) = dataStruct.landing_error(ii);
    
    saccLandingTimeInterval = msEnd(msId):msEnd(msId) + 50; %msEnd(msId) - 25:msEnd(msId) + 25;
    heatmap.(sprintf('sacc_cue_%i_landing', saccCueLoc)).xx = [heatmap.(sprintf('sacc_cue_%i_landing', saccCueLoc)).xx, ...
        vt{ii}.x.position(saccLandingTimeInterval)+ xOffset];
    heatmap.(sprintf('sacc_cue_%i_landing', saccCueLoc)).yy = [heatmap.(sprintf('sacc_cue_%i_landing', saccCueLoc)).yy, ...
        vt{ii}.y.position(saccLandingTimeInterval)+ yOffset];
    switch trialType
        case 'minus_0'
            if saccCueLoc == 1 || saccCueLoc == 5
                saccTrialType = 'vert_valid';
            elseif saccCueLoc == 3 || saccCueLoc == 7
                saccTrialType = 'horz_valid';
            end
        case 'minus_4'
            if saccCueLoc == 1 || saccCueLoc == 5
                saccTrialType = 'vert_invalid';
            elseif saccCueLoc == 3 || saccCueLoc == 7
                saccTrialType = 'horz_invalid';
            end
        case 'minus_2'
            if (saccCueLoc == 1 || saccCueLoc == 5) && (respCueLoc == 3 || respCueLoc == 7)
                saccTrialType = 'vert_horz';
            elseif (saccCueLoc == 3 || saccCueLoc == 7) && (respCueLoc == 1 || respCueLoc == 5)
                saccTrialType = 'horz_vert';
            end
    end
    
    if exist('saccTrialType', 'var')
        counter.(saccTrialType) = counter.(saccTrialType) + 1;
        dataStruct.(saccTrialType).perf(counter.(saccTrialType)) = vt{ii}.Correct;
        dataStruct.(saccTrialType).id(counter.(saccTrialType)) = ii;
        dataStruct.(saccTrialType).gaze_pos(counter.(saccTrialType)) = mean(sqrt(xx_target.^2 + yy_target.^2));
        dataStruct.(saccTrialType).dist_from_target{counter.(saccTrialType)} = distFromTarget;
        dataStruct.(saccTrialType).response_time(counter.(saccTrialType)) = vt{ii}.ResponseTime - vt{ii}.TimeCueON;
    end
end

dataStruct.(trialType).perf(trialCount) = vt{ii}.Correct;
dataStruct.(trialType).id(trialCount) = ii;
dataStruct.(trialType).gaze_pos(trialCount) = mean(sqrt(xx_target.^2 + yy_target.^2));
dataStruct.(trialType).dist_from_target{trialCount} = distFromTarget;
dataStruct.(trialType).response_time(trialCount) = vt{ii}.ResponseTime - vt{ii}.TimeCueON; % Response Time - Time Response Cue ON
dataStruct.(trialType).subj_resp(trialCount) = vt{ii}.Response;
if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
    dataStruct.(trialType).orientation_cued_target(trialCount) = 0;
elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
    dataStruct.(trialType).orientation_cued_target(trialCount) = 1;
end


if strcmp(trialType, 'neutral') || strcmp(trialType, 'cong_neutral')
    if saccCueLoc == 0
        trialType = sprintf('neutral_%i', respCueLoc);
        counter.(trialType) = counter.(trialType) + 1;
%         neutralCount = counter.neutral_1 + counter.neutral_2 + counter.neutral_3 + ...
%             counter.neutral_4 + counter.neutral_5 + counter.neutral_6 + ...
%             counter.neutral_7 + counter.neutral_8 + counter.neutral_9;
        trialCount = counter.(trialType);
        dataStruct.(trialType).perf(trialCount) = vt{ii}.Correct;
        dataStruct.(trialType).id(trialCount) = ii;
        dataStruct.(trialType).gaze_pos(trialCount) = mean(sqrt(xx_target.^2 + yy_target.^2));
        dataStruct.(trialType).dist_from_target{trialCount} = distFromTarget;
        dataStruct.(trialType).response_time(trialCount) = vt{ii}.ResponseTime - vt{ii}.TimeTargetON;
        dataStruct.(trialType).subj_resp(trialCount) = vt{ii}.Response;
        if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
            dataStruct.(trialType).orientation_cued_target(trialCount) = 0;
        elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
            dataStruct.(trialType).orientation_cued_target(trialCount) = 1;
        end
        
        %         saccLandingTimeInterval = msEnd(msId):msEnd(msId) + 50; %msEnd(msId) - 25:msEnd(msId) + 25;
        %         heatmap.sacc_cue_0_landing.xx = [heatmap.sacc_cue_0_landing.xx, ...
        %             vt{ii}.x.position(saccLandingTimeInterval)+ xOffset];
        %         heatmap.sacc_cue_0_landing.yy = [heatmap.sacc_cue_0_landing.yy, ...
        %             vt{ii}.y.position(saccLandingTimeInterval)+ yOffset];
        
        % Drift
        if params.DDPI
            start_time = round(saccCueOn/(1000/330));
            end_time = round(targetOff/(1000/330));
            x = vt{ii}.x.position(start_time:end_time) + xOffset;
            y = vt{ii}.y.position(start_time:end_time) + yOffset;
        else
            x = vt{ii}.x.position(round(saccCueOn):round(targetOff)) + xOffset;
            y = vt{ii}.y.position(round(saccCueOn):round(targetOff)) + yOffset;
        end
        dataStruct = insert_drift_data(dataStruct, trialType, trialCount, x, y);
        dataStruct = insert_drift_data(dataStruct, 'neutral', counter.neutral, x, y);
    end
end

end
