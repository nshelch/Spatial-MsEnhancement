function plot_microsaccade_latency(ft, vt, DelayAfterTOn)

% Microsaccade Latency
figure('Position', [900 350 700 500])
ms_latency = mat2cell([ft.ms_initiation(ft.minus_0.id), ft.ms_initiation(ft.minus_1.id), ...
    ft.ms_initiation(ft.minus_2.id), ft.ms_initiation(ft.minus_3.id), ft.ms_initiation(ft.minus_4.id)], 1);
MultipleHist(ms_latency, 'binfactor', 1, 'samebins','smooth', 'color', 'winter');
h2_ax = axes('Position', get(gca,'Position'));
set(h2_ax,'Color','none')
axis off
hold on
valid_time_start = 280 + (vt{1}.SaccCueTime - vt{1}.DelayTime);
valid_time_end = valid_time_start - 80 + DelayAfterTOn;
stem(valid_time_start, .01, 'gv', 'LineWidth', 1.2,  'MarkerFaceColor', 'g')
stem(valid_time_end, .01, 'rv', 'LineWidth', 1.2, 'MarkerFaceColor', 'r')
set(gca, 'FontSize', 15)
title('Microsaccade Latency')
xlabel('Latency [ms]')
ylabel('Probability')
box off
colormap winter

end