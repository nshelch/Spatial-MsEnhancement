load('./VideoData.mat')

%% Fixation Movie
xx = video.fixation.xx; yy = video.fixation.yy;
xx_ms = video.fixation.xx_ms; yy_ms = video.fixation.yy_ms;
temporal_bins = length(1:5:length(xx));
movie_trial(temporal_bins, 1) = struct('cdata', [], 'colormap', []);
frame_count = 0;
figure(4)
hold on
fill([-3, 3, 3, -3], [3, 3, -3, -3], rgb('Gray'),'EdgeColor','none')
h = viscircles([0,0], 30, 'Color', 'b');
mspos = plot(xx_ms(1), yy_ms(1), '-', 'Color', rgb('DarkRed'));
hsacc = plot(xx(1), yy(1), '-', 'Color', 'k');
currpos = plot(xx(1), yy(1), '.', 'markersize', 8, 'Color',rgb('DarkGreen'));
plot([-5 5], [10 10],'k', 'LineWidth', 5)
text(-7, 13, '10 arcmin', 'FontSize', 12,'FontWeight','bold')
plot([-30 30], [-33 -33],'k', 'LineWidth', 5)
text(-12, -35, '1 degree (60 arcmin)', 'FontSize', 12,'FontWeight','bold')
axis square;
axis([-35 30 -35 30])
axis off
for t = [1:5:length(xx), length(xx)]
    frame_count = frame_count + 1;
    set(hsacc, 'XData', xx(1:t), 'YData', yy(1:t));
    set(currpos, 'XData', xx(t), 'YData', yy(t));
    set(mspos, 'XData', xx_ms(1:t), 'YData', yy_ms(1:t));
    movie_trial(frame_count) = getframe(gcf);
    pause(1e-9);
end
v_trial = VideoWriter('./Documents/VSS/Presentation/FixationMovie', 'Motion JPEG AVI');
open(v_trial);
for ii = 1:temporal_bins
    writeVideo(v_trial, movie_trial(ii));
end
close(v_trial);

%% Faces Movie
xx = video.faces.xx; yy = video.faces.yy;
temporal_bins = length(1:5:length(xx));
movie_trial(temporal_bins, 1) = struct('cdata', [], 'colormap', []);
frame_count = 0;
figure(5)
Im = imread('./1_Neutral_f_45_500px.bmp');
Im = Im(:,:,1); Im = flipud(Im);
imagesc([-176/2 176/2], [-176/2 + 13 176/2 + 13], Im)
axis('xy')
colormap gray
hold on
plot([-176/2 + 8, -176/2 + 8 + 18], [80 80],'k', 'LineWidth', 5)
text(-176/2 + 2, 87, '10 arcmin', 'FontSize', 12, 'FontWeight','bold')
axis off
hsacc = plot(xx(1), yy(1), 'k-', 'LineWidth', 2);
currpos = plot(xx(1), yy(1), 'r.', 'markersize', 10);
for t = [1:5:length(xx), length(xx)]
    frame_count = frame_count + 1;
    set(hsacc, 'XData', xx(1:t), 'YData', yy(1:t));
    set(currpos, 'XData', xx(t), 'YData', yy(t));
    movie_trial(frame_count) = getframe(gcf);
    pause(1e-9);
end
v_trial = VideoWriter('./Documents/VSS/Presentation/FacesMovie', 'Motion JPEG AVI');
v_trial.FrameRate = 50;
open(v_trial);
for ii = 1:temporal_bins
    writeVideo(v_trial, movie_trial(ii));
end
close(v_trial);

%% Spatial Movie
movie_trial = plot_movie_trial(video.spatial.trial, 0.6848);
v_trial = VideoWriter('./Documents/VSS/Presentation/SpatialMovieShort', 'Motion JPEG AVI');
open(v_trial);
for ii = 1:length(movie_trial)
    writeVideo(v_trial, movie_trial(ii));
end
close(v_trial);