switch params.SPLIT
    case 1
        diary(sprintf('%s_RGB%i_split_location_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
        if params.DDPI
            diary(sprintf('%s_RGB%i_DDPI_split_location_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
        end
        diary ON
        fprintf('Subject: %s\n', params.Subject{sub_idx})
        
        % Peripheral condition summary
        fprintf('\nProportion of selected trials: %.2f\n\n', cont.ms_trials/cont.filtered_trials)
        fprintf('Performance (Neutral): %.2f (n = %d) d prime: %.2f \n', ft.neutral.avg_perf, cont.neutral, stats.d_prime.d.neutral)
        fprintf('Performance (-4): %.2f (n = %d) d prime: %.2f \n', ft.minus_4.avg_perf, cont.minus_4, stats.d_prime.d.minus_4)
        fprintf('Performance (-3): %.2f (n = %d) d prime: %.2f \n', ft.minus_3.avg_perf, cont.minus_3, stats.d_prime.d.minus_3)
        fprintf('Performance (-2): %.2f (n = %d) d prime: %.2f \n', ft.minus_2.avg_perf, cont.minus_2, stats.d_prime.d.minus_2)
        fprintf('Performance (-1): %.2f (n = %d) d prime: %.2f \n', ft.minus_1.avg_perf, cont.minus_1, stats.d_prime.d.minus_1)
        fprintf('Performance (0): %.2f (n = %d) d prime: %.2f \n', ft.minus_0.avg_perf, cont.minus_0, stats.d_prime.d.minus_0)
        fprintf('Performance (+1): %.2f (n = %d) d prime: %.2f \n', ft.plus_1.avg_perf, cont.plus_1, stats.d_prime.d.plus_1)
        fprintf('Performance (+2): %.2f (n = %d) d prime: %.2f \n', ft.plus_2.avg_perf, cont.plus_2, stats.d_prime.d.plus_2)
        fprintf('Performance (+3): %.2f (n = %d) d prime: %.2f \n\n', ft.plus_3.avg_perf, cont.plus_3, stats.d_prime.d.plus_3)
        
    case 0
        diary(sprintf('%s_RGB%i_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
        if params.DDPI
            diary(sprintf('%s_RGB%i_DDPI_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
        end
        diary ON
        fprintf('Subject: %s\n', params.Subject{sub_idx})
        
        % Peripheral condition summary
        fprintf('\nProportion of selected trials: %.2f\n\n', cont.ms_trials/cont.filtered_trials)
        fprintf('Performance (Neutral): %.2f (n = %d) d prime: %.2f \n', ft.neutral.avg_perf, cont.neutral, stats.d_prime.d.neutral)
        fprintf('Performance (-0): %.2f (n = %d) d prime: %.2f \n', ft.minus_0.avg_perf, cont.minus_0, stats.d_prime.d.minus_0)
        fprintf('Performance (-1): %.2f (n = %d) d prime: %.2f \n', ft.minus_1.avg_perf, cont.minus_1, stats.d_prime.d.minus_1)
        fprintf('Performance (-2): %.2f (n = %d) d prime: %.2f \n', ft.minus_2.avg_perf, cont.minus_2, stats.d_prime.d.minus_2)
        fprintf('Performance (-3): %.2f (n = %d) d prime: %.2f \n', ft.minus_3.avg_perf, cont.minus_3, stats.d_prime.d.minus_3)
        fprintf('Performance (-4): %.2f (n = %d) d prime: %.2f \n\n', ft.minus_4.avg_perf, cont.minus_4, stats.d_prime.d.minus_4)
end

% Suppresion at fixation point
fprintf('Performance (Valid Neutral): %.2f (n = %d) d prime: %.2f \n', ft.cong_neutral.avg_perf, cont.cong_neutral, stats.d_prime.d.cong_neutral)
fprintf('Performance (Invalid Neutral): %.2f (n = %d) d prime: %.2f \n\n', ft.incong_neutral.avg_perf, cont.incong_neutral, stats.d_prime.d.incong_neutral)

% Ms vs No Ms Condition
fprintf('Performance (Ms): %.2f (n = %d) d prime: %.2f \n', ft.minus_0.avg_perf, cont.minus_0, stats.d_prime.d.minus_0)
fprintf('Performance (No Ms): %.2f (n = %d) d prime: %.2f \n', ft.no_ms_0.avg_perf, cont.no_ms_0, stats.d_prime.d.no_ms_0)
if cont.no_ms_0 > 30  && cont.minus_0 > 30
    fprintf('Significance: %.2f\n', stats.z_test.p_ms_v_noms)
end

% Bias during neutral condition
fprintf('\nPerformance (Neutral 1): %.2f (n = %d) d prime: %.2f \n', ft.neutral_1.avg_perf, cont.neutral_1, stats.d_prime.d.neutral_1)
fprintf('Performance (Neutral 2): %.2f (n = %d) d prime: %.2f \n', ft.neutral_2.avg_perf, cont.neutral_2, stats.d_prime.d.neutral_2)
fprintf('Performance (Neutral 3): %.2f (n = %d) d prime: %.2f \n', ft.neutral_3.avg_perf, cont.neutral_3, stats.d_prime.d.neutral_3)
fprintf('Performance (Neutral 4): %.2f (n = %d) d prime: %.2f \n', ft.neutral_4.avg_perf, cont.neutral_4, stats.d_prime.d.neutral_4)
fprintf('Performance (Neutral 5): %.2f (n = %d) d prime: %.2f \n', ft.neutral_5.avg_perf, cont.neutral_5, stats.d_prime.d.neutral_5)
fprintf('Performance (Neutral 6): %.2f (n = %d) d prime: %.2f \n', ft.neutral_6.avg_perf, cont.neutral_6, stats.d_prime.d.neutral_6)
fprintf('Performance (Neutral 7): %.2f (n = %d) d prime: %.2f \n', ft.neutral_7.avg_perf, cont.neutral_7, stats.d_prime.d.neutral_7)
fprintf('Performance (Neutral 8): %.2f (n = %d) d prime: %.2f \n', ft.neutral_8.avg_perf, cont.neutral_8, stats.d_prime.d.neutral_8)
fprintf('Performance (Neutral 9): %.2f (n = %d) d prime: %.2f \n', ft.neutral_9.avg_perf, cont.neutral_9, stats.d_prime.d.neutral_9)

% Checking trial counts
fprintf('\n Trials discarded for blinks: %d', dt.blinks)
fprintf('\n Trials discarded for no track: %d', dt.no_track)
fprintf('\n Trials discarded for no response: %d', dt.no_response)
fprintf('\n Trials discarded for trial duration: %d', dt.short_trial)
fprintf('\n Trials discarded for saccades: %d', dt.saccades)
fprintf('\n Trials discarded for early microsaccades: %d', dt.early_ms)
fprintf('\n Trials discaded for gaze off center: %d', dt.gaze_off_center)
fprintf('\n Trials discaded manually: %d', dt.manual_discard)
% fprintf('\n Trials discarded for target distance: %d', dt.target_distance)
fprintf('\n Trials discarded for late microsaccade: %d', dt.late_ms)
fprintf('\n Number of valid trials: %d', cont.valid_trials)

fprintf('\n\n Trials prior to filter: %d', ii)
fprintf('\n Total trials: %d \n', cont.valid_trials + sum(cell2mat(struct2cell(dt))))

diary OFF
switch params.SPLIT
    case 1
        if params.DDPI
            movefile(sprintf('%s_RGB%i_DDPI_split_location_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
        else
            movefile(sprintf('%s_RGB%i_split_location_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
        end
    case 0
        if params.DDPI
            movefile(sprintf('%s_RGB%i_DDPI_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
        else
            movefile(sprintf('%s_RGB%i_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
        end
end