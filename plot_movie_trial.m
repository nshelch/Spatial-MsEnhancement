function movie_trial = plot_movie_trial(trial, pxAngle)

fixation_on = round(trial.TimeFixationON);
fixation_off = round(fixation_on + trial.FixationTime);

sacc_cue_on = round(fixation_off + trial.BeepDelayTime);
sacc_cue_off = round(sacc_cue_on + trial.SaccCueTime);

delay_on = sacc_cue_off;
delay_off = delay_on + round(trial.DelayTime - trial.SaccCueTime);

target_on = round(trial.TimeTargetON);
target_off = round(target_on + trial.TargetTime);

mask_on = round(trial.TimeMaskON);
mask_off = round(mask_on + trial.MaskTime);

resp_cue_on = round(trial.TimeCueON);
resp_cue_off = round(resp_cue_on + trial.CueTime);

xx = trial.x.position + trial.xoffset*pxAngle;
yy = trial.y.position + trial.yoffset*pxAngle;

xx(1:288) = NaN; yy(1:288) = NaN;
xx(2100:end) = NaN; yy(2100:end) = NaN;
temporal_bins = length(1:5:length(xx));
movie_trial(temporal_bins, 1) = struct('cdata', [], 'colormap', []);
figure;
box_locations;
plot_stimuli_location(0, 11*pxAngle, 30*pxAngle)
hold on
plot([-25 -15], [23 23],'k', 'LineWidth', 5)
text(-27, 26, '10 arcmin', 'FontSize', 12,'FontWeight','bold')

pastmove = plot(xx(1), yy(1), 'b-','linewidth', 1);
currpos = plot(xx(1), yy(1), 'k.', 'markersize', 10);
already_plotted_sacc = 0;
already_plotted_target = 0;
frame_count = 0;
% title(sprintf('Sacc Cue: %i, Resp Cue: %i', trial.SaccCueType, trial.CueLocation))

for t = [1:5:length(xx), length(xx)]
    frame_count = frame_count + 1;
    if t >= sacc_cue_on && t <= sacc_cue_off
        if ~already_plotted_sacc
%             h = text(-20, 27, 'Saccade Initiation Signal', 'FontSize',15,'FontWeight','bold');
            switch trial.SaccCueType
                case 0
                    sacc_line_up = plot(sacc_cue(1).x, sacc_cue(1).y, 'k-', 'linewidth', 3.5);
                    sacc_line_down = plot(sacc_cue(2).x, sacc_cue(2).y, 'k-', 'linewidth', 3.5);
                    sacc_line_left = plot(sacc_cue(3).x, sacc_cue(3).y, 'k-', 'linewidth', 3.5);
                    sacc_line_right = plot(sacc_cue(4).x, sacc_cue(4).y, 'k-', 'linewidth', 3.5);
                    already_plotted_sacc = 1;
                case 1
                    sacc_line_up = plot(sacc_cue(1).x, sacc_cue(1).y, 'k-', 'linewidth', 3.5);
                    already_plotted_sacc = 1;
                case 3
                    sacc_line_left = plot(sacc_cue(2).x, sacc_cue(2).y, 'k-', 'linewidth', 3.5);
                    already_plotted_sacc = 1;
                case 5
                    sacc_line_down = plot(sacc_cue(3).x, sacc_cue(3).y, 'k-', 'linewidth', 3.5);
                    already_plotted_sacc = 1;
                case 7
                    sacc_line_right = plot(sacc_cue(4).x, sacc_cue(4).y, 'k-', 'linewidth', 3.5);
                    already_plotted_sacc = 1;
            end
        end
    elseif t >= delay_on && t <= delay_off
        switch trial.SaccCueType
            case 0
                delete(sacc_line_up); delete(sacc_line_down);
                delete(sacc_line_left); delete(sacc_line_right);
%                 delete(h);
            case 1
                delete(sacc_line_up);
%                 delete(h);
            case 3
                delete(sacc_line_left);
%                 delete(h);
            case 5
                delete(sacc_line_down);
%                 delete(h);
            case 7
                delete(sacc_line_right);
%                 delete(h);
        end
    elseif t >= target_on && t <= target_off
        switch trial.SaccCueType
            case 0
                delete(sacc_line_up); delete(sacc_line_down);
                delete(sacc_line_left); delete(sacc_line_right);
%                 delete(h);
            case 1
                delete(sacc_line_up);
%                 delete(h);
            case 3
                delete(sacc_line_left);
%                 delete(h);
            case 5
                delete(sacc_line_down);
%                 delete(h);
            case 7
                delete(sacc_line_right);
%                 delete(h);
        end
        if ~already_plotted_target
%             h = text(-15, 27, 'Target Presentation', 'FontSize',15,'FontWeight','bold');
            
            if trial.Target1Orientation == 135
                target1 = plot([box_loc(1).topLeft(1), box_loc(1).bottomRight(1)], [box_loc(1).topLeft(2), box_loc(1).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target1Orientation == 45
                target1 = plot([box_loc(1).topRight(1), box_loc(1).bottomLeft(1)], [box_loc(1).topRight(2), box_loc(1).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target2Orientation == 135
                target2 = plot([box_loc(2).topLeft(1), box_loc(2).bottomRight(1)], [box_loc(2).topLeft(2), box_loc(2).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target2Orientation == 45
                target2 = plot([box_loc(2).topRight(1), box_loc(2).bottomLeft(1)], [box_loc(2).topRight(2), box_loc(2).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target3Orientation == 135
                target3 = plot([box_loc(3).topLeft(1), box_loc(3).bottomRight(1)], [box_loc(3).topLeft(2), box_loc(3).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target3Orientation == 45
                target3 = plot([box_loc(3).topRight(1), box_loc(3).bottomLeft(1)], [box_loc(3).topRight(2), box_loc(3).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target4Orientation == 135
                target4 = plot([box_loc(4).topLeft(1), box_loc(4).bottomRight(1)], [box_loc(4).topLeft(2), box_loc(4).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target4Orientation == 45
                target4 = plot([box_loc(4).topRight(1), box_loc(4).bottomLeft(1)], [box_loc(4).topRight(2), box_loc(4).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target5Orientation == 135
                target5 = plot([box_loc(5).topLeft(1), box_loc(5).bottomRight(1)], [box_loc(5).topLeft(2), box_loc(5).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target5Orientation == 45
                target5 = plot([box_loc(5).topRight(1), box_loc(5).bottomLeft(1)], [box_loc(5).topRight(2), box_loc(5).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target6Orientation == 135
                target6 = plot([box_loc(6).topLeft(1), box_loc(6).bottomRight(1)], [box_loc(6).topLeft(2), box_loc(6).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target6Orientation == 45
                target6 = plot([box_loc(6).topRight(1), box_loc(6).bottomLeft(1)], [box_loc(6).topRight(2), box_loc(6).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target7Orientation == 135
                target7 = plot([box_loc(7).topLeft(1), box_loc(7).bottomRight(1)], [box_loc(7).topLeft(2), box_loc(7).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target7Orientation == 45
                target7 = plot([box_loc(7).topRight(1), box_loc(7).bottomLeft(1)], [box_loc(7).topRight(2), box_loc(7).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target8Orientation == 135
                target8 = plot([box_loc(8).topLeft(1), box_loc(8).bottomRight(1)], [box_loc(8).topLeft(2), box_loc(8).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target8Orientation == 45
                target8 = plot([box_loc(8).topRight(1), box_loc(8).bottomLeft(1)], [box_loc(8).topRight(2), box_loc(8).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            
            if trial.Target9Orientation == 135
                target9 = plot([box_loc(9).topLeft(1), box_loc(9).bottomRight(1)], [box_loc(9).topLeft(2), box_loc(9).bottomRight(2)], 'k-', 'LineWidth', 2.5);
            elseif trial.Target9Orientation == 45
                target9 = plot([box_loc(9).topRight(1), box_loc(9).bottomLeft(1)], [box_loc(9).topRight(2), box_loc(9).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
            end
            already_plotted_target = 1;
        end
    elseif t > mask_on && t < mask_off
        delete(target1); delete(target2); delete(target3); delete(target4);
        delete(target5); delete(target6); delete(target7); delete(target8);
        delete(target9); %delete(h);
    elseif t >= resp_cue_on && t <= resp_cue_off
%         h = text(-12, 27, 'Response Cue', 'FontSize',15,'FontWeight','bold');
        
        plot(resp_cue(trial.CueLocation).x, resp_cue(trial.CueLocation).y, 'k-', 'LineWidth', 3)
    end
    set(pastmove, 'XData', xx(1:t), 'YData', yy(1:t));
    set(currpos, 'XData', xx(t), 'YData', yy(t));
    
    movie_trial(frame_count) = getframe(gcf);
    pause(.001)
    
    
end

%     v_trial = VideoWriter('NS90Minus0Trial609', 'Motion JPEG AVI');
%     open(v_trial);
%     for ii = 1:temporal_bins
%         writeVideo(v_trial, movie_trial(ii));
%     end
%     close(v_trial);

end

