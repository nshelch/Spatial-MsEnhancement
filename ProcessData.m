clear; clc; close all;

% Trial Overview:
%   DelayTime: the duration after trial starts before fixation time starts
%   FixationTime: the duration of the fixation time
%   BeepDelayTime: the variable duration before the saccade cue is presented
%   SaccCueTime: the duration of the saccade cue presentation
%   TargetTime: the duration of the target presentation
%   MaskTime: the duration of the mask after the target appeared
%   CueTime: the duration of the cue presentation

% NS: 0 -> 90
% Z031: 75 -> 95
% Z042: 75
% Z066: 50 -> 50
% Z095: 50

% Fixation: {'NS','Z042','Z031','Z066','Z085'}; [90, 75, 75, 50, 75]
% Periphery: {'NS','Z088','Z031','Z066','Z085'}; [90, 15, 100, 50, 75]

params.Subject = {'NS','Z008','Z031','Z066','Z085'}; %{'NS','Z008','Z031','Z066','Z085'}; % {'NS','Z008','Z031','Z066','Z085'}; %{'NS','Z042','Z031','Z066'}; %{'NS','Z008','Z031','Z066','Z085'};
params.RGB = [90, 15, 100, 50, 75]; % [90, 15, 100, 50, 75]; %[90, 15, 100, 50, 75]; %[90, 75, 75, 50]; %[90, 15, 100, 50, 75];

params.DDPI = 0;
params.FIX_FIGURES = 0;
params.PERI_FIGURES = 0;
params.TABLES = 0;
params.ACROSS_SUBJECT_TABLE = 0;
params.SPLIT = 0;

for sub_idx = 1:length(params.Subject)
    
    if params.DDPI
        load(sprintf('./Data/%s_RGB%i-DDPI.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
    else
        load(sprintf('./Data/%s_RGB%i.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
        if strcmp(params.Subject{sub_idx},'NS') && params.RGB(sub_idx) == 0
            load(sprintf('./Data/%s_RGB%i_combined.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
        end
    end
    [pxAngle] = CalculatePxAngle(vt{1}.Xres, 1550, 600);
    DelayAfterTOn = 450;
    DistThresh = 10;
    DistTarget_tresL = 15;
    DistTarget_tresU = 25;
    MaxMsAmp = 30;
    MinMsAmp = 0;
    
    if strcmp(params.Subject{sub_idx}, 'Z066')
        DistThresh = 18;
    end
    
    % Initialize Counters
    switch params.SPLIT
        case 0
            trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4', ...
                'valid_incong','invalid_cong',  'neutral_cong_ms','neutral_incong_ms', ...
                'neutral','incong_neutral','cong_neutral', ...
                'no_ms_0','no_ms_1','no_ms_2','no_ms_3','no_ms_4','other', ...
                'neutral_1','neutral_2','neutral_3','neutral_4','neutral_5', ...
                'neutral_6','neutral_7','neutral_8','neutral_9',  ...
                'horz_valid','horz_invalid','horz_vert','vert_valid','vert_invalid','vert_horz'};
            plot_split_label = '';
            saved_data_filepath = sprintf('./Data/%s_RGB%i_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx));
            if params.DDPI
               saved_data_filepath = sprintf('./Data/%s_RGB%i_DDPI_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx)); 
            end
        case 1
            trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4', ...
                'plus_1','plus_2','plus_3', ...
                'valid_incong','invalid_cong',  'neutral_cong_ms','neutral_incong_ms', ...
                'neutral','incong_neutral','cong_neutral', ...
                'no_ms_0','no_ms_1','no_ms_2','no_ms_3','no_ms_4','other', ...
                'neutral_1','neutral_2','neutral_3','neutral_4','neutral_5', ...
                'neutral_6','neutral_7','neutral_8','neutral_9', ...
                'horz_valid','horz_invalid','horz_vert','vert_valid','vert_invalid','vert_horz'};
            plot_split_label = '_Split';
            saved_data_filepath = sprintf('./Data/%s_RGB%i_split_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx));
            if params.DDPI
               saved_data_filepath = sprintf('./Data/%s_RGB%i_split_DDPI_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx)); 
            end
    end
    
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        ft.(cur_trial) = filteredTrialsStruct();
        cont.(cur_trial) = 0;
        heatmap.(cur_trial).xx(1) = NaN;
        heatmap.(cur_trial).yy(1) = NaN;
    end
    % Trial Statistics Structure
    dt = discardedTrialsStruct();
    cont.fix_ms_trial = 0;
    cont.fix_drift_trial = 0;
    cont.ms_trials = 0;
    cont.filtered_trials = 0;
    cont.valid_trials = 0;
    
    % Initialize Heatmaps
    for sacc_idx = [0,1,3,5,7]
        heatmap.(sprintf('sacc_cue_%i_target', sacc_idx)).xx = [];
        heatmap.(sprintf('sacc_cue_%i_target', sacc_idx)).yy = [];
        heatmap.(sprintf('sacc_cue_%i_landing', sacc_idx)).xx = [];
        heatmap.(sprintf('sacc_cue_%i_landing', sacc_idx)).yy = [];
    end
    
    for ii = 1:length(vt)
        
        if vt{ii}.CueLocation == 0
            continue;
        end
        
        if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
            ft.orientation_cued_target(ii) = 0;
        elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
            ft.orientation_cued_target(ii) = 1;
        end
        
        % Finds all microsaccades that occured after saccade cue signal
        sacc_cue_on = vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime;
        idx_valid_ms = find(vt{ii}.microsaccades.start > sacc_cue_on);
        
        % first microsaccade after the saccade cue signal in all trials
        if ~isempty(idx_valid_ms)
            % ms_latency is a vector which tells how long it took the subject to
            % initiate a microsaccade
            ft.ms_initiation(ii) = (vt{ii}.microsaccades.start(idx_valid_ms(1)) - sacc_cue_on);
        end
        
        [ft, cont] = insert_fixation_data(ft, params, cont, vt, ii);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % eliminate trials with no tracks, blinks, large saccades, no response,
        % and too short trial duration
        [filter_output, dt] = filter_trial(vt{ii}, dt, params.DDPI);
        
        if filter_output
            
            % Trials in which the subject responded and passed a filter
            cont.filtered_trials = cont.filtered_trials + 1;
            
            % eliminates microsaccades that occurred at the very beginning
            % of the trial and were used to recenter the gaze
            id = find(vt{ii}.microsaccades.start > 50);
            ms_start = vt{ii}.microsaccades.start(id);
            ms_end = vt{ii}.microsaccades.duration(id) + ms_start;
            ms_amp = vt{ii}.microsaccades.amplitude(id);
            sacc_cue_location = vt{ii}.SaccCueType;
            resp_cue_location = vt{ii}.CueLocation;
            
            % find the microsaccades/drifts performed 80 - 300 ms from target on
            % these two numbers can be parameters at the beginning of the code script
            target_on = vt{ii}.TimeTargetON;
            target_off = round(vt{ii}.TimeTargetON + vt{ii}.TargetTime);
            
            idx_valid_ms = isBetween((target_on + 80), ms_start, (target_on + DelayAfterTOn)); % ms occured during period of interest
            idx_no_ms = isBetween((target_on - 100), ms_start, vt{ii}.TimeCueON); % no ms occuring during slightly wider period of interest
            idx_ms_clear = isBetween((sacc_cue_on - 80), ms_start, target_on); % ms occuring prior to period of interest
            
            % check that the average gaze position when the target is on is
            % close to 0
            if params.DDPI
                start_time = round((target_on - 50) / (1000/330));
                end_time = round(target_off / (1000/330));
                xx_target = vt{ii}.x.position(start_time:end_time) + vt{ii}.xoffset * pxAngle;
                yy_target = vt{ii}.y.position(start_time:end_time) + vt{ii}.yoffset * pxAngle;
            else
                xx_target = vt{ii}.x.position(round(target_on - 50):target_off) + vt{ii}.xoffset * pxAngle;
                yy_target = vt{ii}.y.position(round(target_on - 50):target_off) + vt{ii}.yoffset * pxAngle;
            end
            ft.dist_from_target_x(ii) = mean(xx_target); ft.dist_from_target_y(ii) = mean(yy_target);
            
            % distance from the cued location
            dist_from_target = target_classification(xx_target, yy_target, resp_cue_location, double(vt{ii}.TargetOffsetpx), pxAngle);
            ft.avg_dist_from_target(ii) = mean(dist_from_target);
            ft.gaze_loc = mean(sqrt(xx_target.^2 + yy_target.^2));
            
            %             plot_eye_trace(xx_target, yy_target, avg_dist_from_target(ii), Dist_thresh, sacc_cue_location, resp_cue_location, pxAngle)
            
            if isempty(idx_ms_clear) && ...
                    mean(ft.gaze_loc) < DistThresh %%&&...
                %                     mean(dist_from_target) > DistTarget_tresL && ...
                %                     mean(dist_from_target) < DistTarget_tresU
                
                % MICROSACCADE PERFORMED
                if ~isempty(idx_valid_ms) && ...
                        ms_amp(idx_valid_ms(1)) < MaxMsAmp && ...
                        ms_amp(idx_valid_ms(1)) > MinMsAmp
                    
                    msId = find(vt{ii}.microsaccades.amplitude == ms_amp(idx_valid_ms(1)));
                    [ft, cont, heatmap] = insert_trial_data(ft, params, cont, heatmap, params.SPLIT, vt, ii, msId, xx_target, yy_target);
                    
                    % NO MICROSACCADE PERFORMED
                elseif isempty(idx_no_ms) %&& isempty(idx_ms_clear)
                    msId = [];
                    [ft, cont, heatmap] = insert_trial_data(ft, params, cont, heatmap, params.SPLIT, vt, ii, msId, xx_target, yy_target);
                else
                    dt.late_ms = dt.late_ms + 1;
                end
            else
                if ~isempty(idx_ms_clear)
                    dt.early_ms = dt.early_ms + 1;
                    
                elseif ~(mean(ft.gaze_loc) < DistThresh)
                    dt.gaze_off_center = dt.gaze_off_center + 1;
                    
                    %                 elseif ~(mean(dist_from_target) > DistTarget_tresL) || ~(mean(dist_from_target) < DistTarget_tresU)
                    %                     dt.target_distance = dt.target_distance + 1;
                end
            end
        end
    end
    
    ft.response_time = cellfun(@(z) z(:).ResponseTime - z(:).TimeCueON - z(:).CueTime, vt);
    ft.sacc_cue_type = cellfun(@(z) z(:).SaccCueType, vt);
    ft.cue_location = cellfun(@(z) z(:).CueLocation, vt);
    ft.subj_resp = cellfun(@(z) z(:).Response, vt);
    ft.performance = cellfun(@(z) z(:).Correct, vt);
    ft.counter = cont;
    ft.heatmap = heatmap;
    
    neutral_trial_types = {'neutral_1','neutral_2','neutral_3','neutral_4','neutral_5', ...
        'neutral_6','neutral_7','neutral_8','neutral_9'};
    
    % Group based on sacc cue/resp cue locations
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        ft.(cur_trial).avg_perf = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
        if cont.(cur_trial) ~= 0 && any(strcmp(neutral_trial_types, cur_trial))
            ft.(cur_trial).drift.avg_perf = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
            ft.(cur_trial).drift.avg_curvature = nanmean(ft.(cur_trial).drift.curvature);
            ft.(cur_trial).drift.avg_speed = nanmean(ft.(cur_trial).drift.speed);
            ft.(cur_trial).drift.avg_span = nanmean(ft.(cur_trial).drift.span);
            ft.(cur_trial).avg_resp_time = nanmean(ft.(cur_trial).response_time);
            % Diffusion Coef
            [~,~, ft.(cur_trial).drift.dcoef, ~, ft.(cur_trial).drift.dsq, ft.(cur_trial).drift.singleSeg, ft.(cur_trial).drift.timeDsq, ~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.position);
            ft.(cur_trial).drift.indv_dcoef = getIndvDcoef(ft.(cur_trial).drift.singleSeg);
            ft.(cur_trial).drift.sem_dcoef = nansem(ft.(cur_trial).drift.indv_dcoef, length(ft.(cur_trial).drift.indv_dcoef));
            
            % Diffusion Coef in X
            [~,~, ft.(cur_trial).drift.dcoef_x, ~, ~, ft.(cur_trial).drift.singleSegX, ~, ~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.positionX);
            ft.(cur_trial).avg_dcoef_x = ft.(cur_trial).drift.dcoef_x/2;
            ft.(cur_trial).drift.indv_dcoef_x = (getIndvDcoef(ft.(cur_trial).drift.singleSegX) / 2);
            ft.(cur_trial).drift.sem_dcoef_x = nansem(ft.(cur_trial).drift.indv_dcoef_x, length(ft.(cur_trial).drift.indv_dcoef_x));
            
            % Diffusion Coef in Y
            [~,~, ft.(cur_trial).drift.dcoef_y, ~, ~, ft.(cur_trial).drift.singleSegY, ~, ~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.positionY);
            ft.(cur_trial).avg_dcoef_y = ft.(cur_trial).drift.dcoef_y/2;
            ft.(cur_trial).drift.indv_dcoef_y = (getIndvDcoef(ft.(cur_trial).drift.singleSegY) / 2);
            ft.(cur_trial).drift.sem_dcoef_y = nansem(ft.(cur_trial).drift.indv_dcoef_y, length(ft.(cur_trial).drift.indv_dcoef_y));
        end
    end
    
    for fixId = {'fixation','neutral'}
        for flds = {'span','curvature','speed','vel_x','vel_y','varx','vary'}
            avgFlds = sprintf('avg_%s',flds{1});
            semFlds = sprintf('sem_%s',flds{1});
            ft.(fixId{1}).drift.(avgFlds) = nanmean(ft.(fixId{1}).drift.(flds{1}));
            ft.(fixId{1}).drift.(semFlds) = nansem(ft.(fixId{1}).drift.(flds{1}), length(ft.(fixId{1}).drift.(flds{1})));
        end
        % Diffusion Coef
        [~,~, ft.(fixId{1}).drift.dcoef, ~, ft.(fixId{1}).drift.dsq, ft.(fixId{1}).drift.singleSeg, ft.(fixId{1}).drift.timeDsq, ~] = ...
            CalculateDiffusionCoef(ft.(fixId{1}).drift.position);
        ft.(fixId{1}).drift.indv_dcoef = getIndvDcoef(ft.(fixId{1}).drift.singleSeg);
        ft.(fixId{1}).drift.sem_dcoef = nansem(ft.(fixId{1}).drift.indv_dcoef, length(ft.(fixId{1}).drift.indv_dcoef));
        % Diffusion Coef in X
        [~,~, ft.(fixId{1}).drift.dcoef_x, ~, ~, ft.(fixId{1}).drift.singleSegX, ~, ~] = ...
            CalculateDiffusionCoef(ft.(fixId{1}).drift.positionX);
        ft.(fixId{1}).dcoef_x = ft.(fixId{1}).drift.dcoef_x/2;
        ft.(fixId{1}).drift.indv_dcoef_x = (getIndvDcoef(ft.(fixId{1}).drift.singleSegX) / 2);
        ft.(fixId{1}).drift.sem_dcoef_x = nansem(ft.(fixId{1}).drift.indv_dcoef_x, length(ft.(fixId{1}).drift.indv_dcoef_x));
        
        % Diffusion Coef in Y
        [~,~, ft.(fixId{1}).drift.dcoef_y, ~, ~, ft.(fixId{1}).drift.singleSegY, ~, ~] = ...
            CalculateDiffusionCoef(ft.(fixId{1}).drift.positionY);
        ft.(fixId{1}).dcoef_y = ft.(fixId{1}).drift.dcoef_y/2;
        ft.(fixId{1}).drift.indv_dcoef_y = (getIndvDcoef(ft.(fixId{1}).drift.singleSegY) / 2);
        ft.(fixId{1}).drift.sem_dcoef_y = nansem(ft.(fixId{1}).drift.indv_dcoef_y, length(ft.(fixId{1}).drift.indv_dcoef_y));
        
    end
    ft.fixation.avg_ms_rate = nanmean(ft.fixation.ms_rate);
    
    for saccLandingPos = [0,1,3,5,7]
        limit = 30;
        n_bins = 50;
        sacc_target = sprintf('sacc_cue_%i_landing', saccLandingPos);
        result = histogram2(ft.heatmap.(sacc_target).xx(:)', ft.heatmap.(sacc_target).yy(:)', [-limit, limit, n_bins; -limit, limit, n_bins]);
        result = result./max(max(result));
        result(result == 0) = NaN;
        ft.heatmap.(sacc_target).map = result;
    end
    % plot(ft.minus_0.id, 1,'ks','MarkerSize',2)
    % hold on
    % plot(ft.minus_4.id, 2,'ks','MarkerSize',2)
    % plot(ft.no_ms_0.id, 3,'ks','MarkerSize',2)
    % plot(ft.no_ms_4.id, 4,'ks','MarkerSize',2)
    % set(gca,'YTick',[1,2,3,4],'YTickLabel',{'Cong.','Incong.','Fix. cong','Fix. incong'})
    % ylim([0.5, 4.5])
    
    stats = statisticalTests(ft, trial_types);
    if params.FIX_FIGURES
        FixationGraphs;
    end
    if params.PERI_FIGURES && ~params.SPLIT
        PeripheralGraphs;
    end
    if params.TABLES
        generateTables; clc;
    end
    PrintSummaries
    save(saved_data_filepath, 'ft', 'stats', 'dt');
    
    if length(params.Subject) > 1
        close all; clc; clearvars -except sub_idx params
    end
end

if params.ACROSS_SUBJECT_TABLE
    
    params.Subject = {'NS','Z008','Z031','Z066','Z085'};
    params.RGB = [90, 15, 100, 50, 75];
    subRowNames = cell(1, length(params.Subject));
    for sub_idx = 1:length(params.Subject)
        load(sprintf('./Data/%s_RGB%i_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
        trialStats(sub_idx,:) = [cell2mat(struct2cell(dt))', ft.counter.valid_trials, sub_idx];
        subRowNames{sub_idx} = sprintf('%s %i',params.Subject{sub_idx}, params.RGB(sub_idx));
    end
    
    tableParams.dataFormat = {'%i'};
    tableParams.completeTable = 1;
    tableParams.transposeTable = 0;
    tableParams.tableLabel = 'DiscardedTrials';
    tableParams.tableCaption = 'Trial Statistics';
    
    trialStruct = struct('Blinks', trialStats(:,1), ...
        'NoTracks', trialStats(:,2), ...
        'NoResp', trialStats(:,3), ...
        'TrialDur', trialStats(:,4), ...
        'Saccades', trialStats(:,5), ...
        'EarlyMs', trialStats(:,6), ...
        'LateMs', trialStats(:,7), ...
        'GazeOffCenter', trialStats(:,8), ...
        'Manual', trialStats(:,9), ...
        'ValidTrials', trialStats(:,10), ...
        'TotalTrials', trialStats(:,11));
    discardedTrialsTable = struct2table(trialStruct, 'rowNames', subRowNames);
    finalTableOutput(discardedTrialsTable, tableParams, './Documents/Peripheral Report', 'TrialStatistics.txt')
end
