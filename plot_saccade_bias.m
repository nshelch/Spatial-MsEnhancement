function plot_saccade_bias(meanVector, stdVector, trialNames)

errorbar(meanVector, stdVector, 'ok', 'LineStyle', 'none', 'LineWidth', 2)
trialNames2 = cellfun(@(x) strrep(x,'_','\newline'), trialNames,'UniformOutput',false);
set(gca, 'FontSize', 15, 'XTick', 1:2, 'XTickLabel', trialNames2)
xlim([.5 2.5])

end