% Distance between two boxes (center to center):
% radius (center to center): 20.538
% -1: 15.7191
% -2: 29.0451
% -3: 37.9493
% -4: 41.076

% Distance between two boxes (bottom right corner (1) to top left (2,3,4)):
% radius (edge to edge):13.005
% -1: 7.1582
% -2: 18.3918
% -3: 28.4048
% -4: 33.54

pngFilepath = './Documents/Figures/PNG';
epscFilepath = './Documents/Figures/EPSC';

% Microsaccade Latency
plot_microsaccade_latency(ft, vt, DelayAfterTOn)
saveFigure(sprintf('MicrosaccadeLatency/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Distance From Target
plot_distance_from_target(ft, 1, 1)
figure(300)
saveFigure(sprintf('DistanceFromTargetSaccCue/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})
figure(400)
saveFigure(sprintf('DistanceFromTargetRespCue/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})
figure(500)
saveFigure(sprintf('DistanceFromTargetBoxplot/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})
figure(600)
saveFigure(sprintf('DistanceFromTargetScatter/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Heatmaps for before and after microsaccade execution
plot_saccade_heatmaps(ft)
saveFigure(sprintf('SaccadeExecutionHeatmap/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Peripheral Accuracy (Percentage)
figure('Position', [300, 200, 1100, 600]);
subplot(1,2,1) % Peripheral Condition
trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral'};
[msPerf, msStdPerf, msDprime, msCiDprime, msCounter] = trialTypeLoop(trial_types, ft, stats);
plot_trial_performance(msPerf, msStdPerf, params.SPLIT, 'Peripheral')
subplot(1,2,2) % Fixation Condition
trial_types = {'cong_neutral','incong_neutral'};
[fix_perf, fix_std_perf, fix_dprime_vector, fix_ci_vector, ~] = trialTypeLoop(trial_types, ft, stats);
plot_fixation_performance(fix_perf, fix_std_perf, 'Fixation', {'Valid','Invalid'})
saveFigure(sprintf('Performance/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Peripheral D-prime
figure('Position', [300, 200, 1100, 600]);
subplot(1,2,1) % Peripheral Condition
plot_trial_dprime(msDprime, msCiDprime, params.SPLIT, 'Peripheral')
subplot(1,2,2) % Fixation Condition
plot_fixation_dprime(fix_dprime_vector, fix_ci_vector, 'Fixation', {'Valid','Invalid'})
saveFigure(sprintf('Dprime/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Heatmaps for Sensitivity (Peripheral)
if params.SPLIT
    trial_types = {'minus_0','plus_1','plus_2','plus_3','minus_4', 'minus_3', 'minus_2', 'minus_1', 'incong_neutral'};
    [~, ~, dprime_vector, ~, sensitivity_counter] = trialTypeLoop(trial_types, ft, stats);
else
    trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4', 'minus_3', 'minus_2', 'minus_1', 'incong_neutral'};
    [~, ~, dprime_vector, ~, sensitivity_counter] = trialTypeLoop(trial_types, ft, stats);
end
plotDprimeMap(dprime_vector, sensitivity_counter, sprintf('%s Sensitivity', params.Subject{sub_idx}))
saveFigure(sprintf('DprimeMap/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

plotDrimeHeatmap(dprime_vector, sprintf('%s Sensitivity', params.Subject{sub_idx}))
saveFigure(sprintf('DprimeInterpHeatmap/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

plotDrimeInterpHeatmap(dprime_vector, sprintf('%s Sensitivity', params.Subject{sub_idx}))
saveFigure(sprintf('DprimeHeatmap/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% real(log(dprime_vector*100))

% No Microsaccade Dprime (Peripheral)
trial_types = {'no_ms_0', 'no_ms_1', 'no_ms_2', 'no_ms_3', 'no_ms_4', 'neutral'};
[noMsPerf, noMsStd, noMsDprime, noMsCiDprime, noMsCounter] = trialTypeLoop(trial_types, ft, stats);
plot_ms_no_ms_comparison(msPerf, msStdPerf, msDprime, msCiDprime, msCounter, noMsPerf, noMsStd, noMsDprime, noMsCiDprime, noMsCounter)
saveFigure(sprintf('MsNoMsComparison/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Ms No Ms Comparison with Neutral
trial_types = {'minus_0', 'no_ms_0', 'incong_neutral', 'cong_neutral'};
[~, ~, msNoMsNeutralDprime, msNoMsNeutralCiDprime, msNoMsNeutralCounter] = trialTypeLoop(trial_types, ft, stats);
figure;
errorbar(msNoMsNeutralDprime, msNoMsNeutralCiDprime, 'ok','LineStyle', 'none', 'LineWidth',2)
ylabel("Sensitivity [d']")
set(gca, 'XTick', 1:4, 'Xlim', [0.5 4.5], 'FontSize', 12, ...
 'XTickLabel', {'Cong.','Fix. Cong.','Incong. Neutral','Cong. Neutral'})
xtickangle(315); box off; 
saveFigure(sprintf('MsNoMsNeutralComparison/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Saccade Horizontal Vertical Bias
trial_types = {'horz_invalid', 'vert_horz', 'vert_invalid', 'horz_vert'};
[~, ~, sacc_dprime_vector, sacc_ci_vector, ~] = trialTypeLoop(trial_types, ft, stats);
figure('position', [200, 50, 1500, 900])
subplot(1,2,1)
plot_saccade_bias(sacc_dprime_vector(1:2), sacc_ci_vector(1:2), trial_types(1:2))
title('Horizontal Saccade Bias')
ylabel("Sensitivity [d']")
subplot(1,2,2)
plot_saccade_bias(sacc_dprime_vector(3:4), sacc_ci_vector(3:4), trial_types(3:4))
ylabel("Sensitivity [d']")
title('Vertical Saccade Bias')
saveFigure(sprintf('SaccadeBias/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

% Neutral Trials w/ Microsaccade Performance
trial_types = {'neutral_cong_ms', 'neutral_incong_ms'};
[msPerf, msStdPerf, dprime_vector, ci_vector, ~] = trialTypeLoop(trial_types, ft, stats);
figure('position', [500, 200, 1000, 700])
subplot(1,2,1)
plot_fixation_performance(msPerf, msStdPerf, '', {'Cong. Ms', 'Incong. Ms'})
subplot(1,2,2)
plot_fixation_dprime(dprime_vector, ci_vector, '', {'Cong. Ms', 'Incong. Ms'})
supertitle('Peripheral (No Microsaccade Performed)','FontSize',15)
saveFigure(sprintf('NeutralMs/%s_RGB%i', params.Subject{sub_idx}, params.RGB(sub_idx)), {pngFilepath, epscFilepath}, {'png','epsc'})

