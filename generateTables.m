colNames = {'Trials', 'Dprime', 'Accuracy'};
subjCode = sprintf('%s_RGB%i', Subject{sub_idx}, rgb_value(sub_idx));

% Individual subject sensitivity table
tableParams.dataFormat = {'%i', 1, '%.2f', 1, '%.1f', 1};
tableParams.completeTable = 0;
tableParams.transposeTable = 0;
tableParams.tableLabel = sprintf('%s_ms', subjCode);
tableParams.tableCaption = 'Sensitivity as a function of distance';
rowNames = {'0 arcmin', '16 arcmin', '29 arcmin', '38 arcmin', '42 arcmin', 'neutral'};
trial_types = {'minus_0','minus_1','minus_2','minus_3','minus_4','neutral'};
[accuracyMs, ~, dprimeMs, ~, msCounter] = trialTypeLoop(trial_types, ft, stats);
sensitivityTable = table(msCounter', dprimeMs', accuracyMs'*100, 'rowNames', rowNames, 'VariableNames', colNames);
finalTableOutput(sensitivityTable, tableParams, './Documents/Peripheral Report/TableLatexFiles', sprintf('%s_ms.txt', subjCode))

% Individual subject neutral sensitivity table
tableParams.dataFormat = {'%i', 1, '%.2f', 1, '%.1f', 1};
tableParams.completeTable = 1;
tableParams.transposeTable = 1;
tableParams.tableLabel = sprintf('%s_neutral', subjCode);
tableParams.tableCaption = 'Sensitivity of the visual field at fixation';
rowNames = {'neutral 1', 'neutral 2', 'neutral 3', 'neutral 4', 'neutral 5', 'neutral 6', 'neutral 7', 'neutral 8', 'neutral 9'};
trial_types = {'neutral_1', 'neutral_2', 'neutral_3', 'neutral_4', 'neutral_5',  'neutral_6', 'neutral_7', 'neutral_8', 'neutral_9'};
[accuracyNeutral, ~, dprimeNeutral, ~, neutralCounter] = trialTypeLoop(trial_types, ft, stats);
neutralTable = table(neutralCounter', dprimeNeutral', accuracyNeutral'*100, 'rowNames', rowNames, 'VariableNames', colNames);
finalTableOutput(neutralTable, tableParams, './Documents/Fixation Report/TableLatexFiles', sprintf('%s_neutral.txt', subjCode))