function [avgPerf, stdPerf, indvDprime, ciDprime, counter] = trialTypeLoop(trialTypes, ft, stats)

[avgPerf, stdPerf, indvDprime, ciDprime, counter] = deal(NaN(1, length(trialTypes)));

for trialIdx = 1:length(trialTypes)
    curTrial = trialTypes{trialIdx};
    avgPerf(trialIdx) = ft.(curTrial).avg_perf;
    stdPerf(trialIdx) = sqrt((ft.(curTrial).avg_perf * ...
        abs(ft.(curTrial).avg_perf - 1)) / length(ft.(curTrial).perf));
    indvDprime(trialIdx) = stats.d_prime.d.(curTrial);
    ciDprime(trialIdx) = stats.d_prime.ci.(curTrial);
    counter(trialIdx) = ft.counter.(curTrial);   
end

end