subject = {'Z066'};
rgbValue = 50;
load(sprintf('./Data/%s_RGB%i_summary.mat', subject{1}, rgbValue));
tmp = dir(sprintf('./EyeRIS/UnprocessedData/%s/RGB%i/', subject{1}, rgbValue));
destDir = sprintf('./EyeRIS/UnprocessedData/%s/RGB%i/NeutralOnly', subject{1}, rgbValue);

neutralId = sort([ft.neutral_1.id, ft.neutral_2.id, ft.neutral_3.id, ft.neutral_4.id, ft.neutral_5.id, ft.neutral_6.id, ft.neutral_7.id, ft.neutral_8.id, ft.neutral_9.id]);

for fileId = 1:length(neutralId)
   eisFilename = tmp(neutralId(fileId) + 2).name;
   eisDir = sprintf('./EyeRIS/UnprocessedData/%s/RGB%i/%s', subject{1}, rgbValue, eisFilename);
   movefile(eisDir, destDir)    
end
