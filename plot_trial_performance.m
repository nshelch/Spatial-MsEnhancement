function plot_trial_performance(v_perf, std_perf, splitLocation, titleStr)

if splitLocation
    xLim = -45;
    distances = [-41, -38, -29, -16, 0, 16, 29, 38, 41];
else
    xLim = -5;
    distances = [0, 16, 29, 38, 41];
end

errorbar(distances, v_perf(1:end-1), std_perf(1:end-1), 'ok', 'LineStyle', 'none', 'LineWidth', 2)
hold on
plot([xLim 45], [v_perf(end), v_perf(end)] , 'r', 'LineWidth',2)
plot([xLim 45], [v_perf(end) + std_perf(end),  v_perf(end) + std_perf(end)] , 'r--', 'LineWidth',2)
plot([xLim 45], [v_perf(end) - std_perf(end), v_perf(end) - std_perf(end)] , 'r--', 'LineWidth',2)
title(titleStr)
set(gca, 'FontSize', 15, 'YTick', .3:.1:1)
xticks(distances)
ylabel('Proportion Correct')
xlabel('Distance from saccade cue location')
ylim([.3, 1])
xlim([xLim 45])
box off

end