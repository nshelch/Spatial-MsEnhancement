%% Split Dprime Figure
xLim = -45;
distances = [-41, -38, -29, -16, 0, 16, 29, 38, 41];
dprimeVec = [summary.minus_4.dprime, summary.minus_3.dprime, summary.minus_2.dprime, ...
    summary.minus_1.dprime, summary.minus_0.dprime, summary.plus_1.dprime, ...
    summary.plus_2.dprime, summary.plus_3.dprime, summary.minus_4.dprime];
se_dprimeVec = [summary.minus_4.se_dprime, summary.minus_3.se_dprime, summary.minus_2.se_dprime, ...
    summary.minus_1.se_dprime, summary.minus_0.se_dprime, summary.plus_1.se_dprime, ...
    summary.plus_2.se_dprime, summary.plus_3.se_dprime, summary.minus_4.se_dprime];
figure('position', [700 350 1200 500])
errorbar(distances, dprimeVec, se_dprimeVec, 'ok', 'LineStyle', 'none', 'LineWidth', 2, ...
'MarkerFaceColor','w','Markersize',8)
hold on
plot([xLim 45], [summary.neutral.dprime, summary.neutral.dprime] , 'color', rgb('DarkRed'), 'LineWidth',2)
plot([xLim 45], [summary.neutral.dprime + summary.neutral.se_dprime,  summary.neutral.dprime + summary.neutral.se_dprime] , ...
'color', rgb('DarkRed'), 'LineStyle', '--', 'LineWidth', 2)
plot([xLim 45], [summary.neutral.dprime - summary.neutral.se_dprime, summary.neutral.dprime - summary.neutral.se_dprime] , ...
'color', rgb('DarkRed'), 'LineStyle', '--', 'LineWidth', 2)
set(gca, 'FontSize', 15, 'YTick', 0:.5:2)
xticks(distances)
ylabel("Sensitivity [d']")
xlabel('Distance from saccade target location [arcmin]')
ylim([-0.25 2])
xlim([xLim 45])
box off

% Add line of best fit
p = polyfit(distances, dprimeVec, 8);
y = polyval(p, distances);
plot(distances, y, 'color', rgb('DarkGreen'), 'LineWidth', 2)
loDprime = dprimeVec - se_dprimeVec; hiDprime = dprimeVec + se_dprimeVec;
hf1 = nan(length(loDprime)-1, 1);
for jj = 1:(length(loDprime)-1)
    hf1(jj) = fill([distances(jj:jj+1) distances(jj+1:-1:jj)], [loDprime(jj:jj+1) hiDprime(jj+1:-1:jj)], 'g');
end
hflFaceColor = rgb('DarkGreen');
set(hf1, 'FaceAlpha', .3, 'EdgeColor', 'none');
set(hf1, 'FaceColor', hflFaceColor);

print('-djpeg','./Documents/VSS/Presentation/Dprime.jpeg');
print('-depsc','./Documents/VSS/Presentation/Dprime.epsc');

%% Sensitivity Heatmap
dprimeVec = [summary.minus_0.dprime, summary.plus_1.dprime, summary.plus_2.dprime, ...
    summary.plus_3.dprime, summary.minus_4.dprime, summary.minus_3.dprime, ...
    summary.minus_2.dprime, summary.minus_1.dprime, summary.incong_neutral.dprime];
plot_interp_sensitivity(dprimeVec, '')
figure(100)
print('-djpeg','./Documents/VSS/Presentation/DprimeHeatmap.jpeg');
print('-depsc','./Documents/VSS/Presentation/DprimeHeatmap.epsc');

dprimeVec = [summary.minus_0.dprime, summary.plus_1.dprime, summary.plus_2.dprime, ...
    summary.plus_3.dprime, summary.minus_4.dprime, summary.minus_3.dprime, ...
    summary.minus_2.dprime, summary.minus_1.dprime, 0];
plot_interp_sensitivity(dprimeVec, '')
figure(200)

respTimeVec = [summary.minus_0.resp_time, summary.plus_1.resp_time, summary.plus_2.resp_time, ...
    summary.plus_3.resp_time, summary.minus_4.resp_time, summary.minus_3.resp_time, ...
    summary.minus_2.resp_time, summary.minus_1.resp_time, summary.incong_neutral.resp_time];
plot_interp_sensitivity(respTimeVec, '')
figure(200)
print('-djpeg','./Documents/VSS/Presentation/RespTime.jpeg');
print('-depsc','./Documents/VSS/Presentation/RespTime.epsc');


%% Saccade Landing Locations
figure
% plot_stimuli_location(0, 11*.6848, 30*.6848);
% GenerateMap(indv.saccCue3Heatmap, 30, .6848)
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
pcolor(linspace(-30, 30, size(indv.saccCue3Heatmap.landing, 1)), linspace(-30, 30, size(indv.saccCue3Heatmap.landing, 1)), indv.saccCue3Heatmap.landing');
shading interp
axis square
box off
print('-dbmp','./Documents/VSS/Presentation/SaccLanding3.bmp');
print('-depsc','./Documents/VSS/Presentation/SaccLanding3.epsc');

figure
% plot_stimuli_location(0, 11*.6848, 30*.6848);
% GenerateMap(indv.saccCue3Heatmap, 30, .6848)
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
pcolor(linspace(-30, 30, size(indv.saccCue3Heatmap.starting, 1)), linspace(-30, 30, size(indv.saccCue3Heatmap.starting, 1)), indv.saccCue3Heatmap.starting');
shading interp
axis square
box off
print('-dbmp','./Documents/VSS/Presentation/SaccStarting3.bmp');
print('-depsc','./Documents/VSS/Presentation/SaccLanding3.epsc');

%% Ms vs No Ms Condition
figure('Position', [900 350 700 500])
errorbar([summary.minus_0.dprime, summary.no_ms_0.dprime], [summary.minus_0.se_dprime, summary.no_ms_0.se_dprime], ...
'ok', 'LineStyle', 'none', 'LineWidth', 2, ...
'MarkerFaceColor','w','Markersize',8)
set(gca, 'XTick', 1:2, 'XTickLabel', {'Ms','No Ms'}, 'FontSize', 15)
ylabel("Sensitivity [d']")
xlim([.5 2.5])
box off
[~, p] = ttest(indv.minus_0.dprime, indv.no_ms_0.dprime)

xLim = -45;
distances = [-41, -38, -29, -16, 0, 16, 29, 38, 41];
dprimeVec = [summary.minus_4.dprime, summary.minus_3.dprime, summary.minus_2.dprime, ...
    summary.minus_1.dprime, summary.no_ms_0.dprime, summary.plus_1.dprime, ...
    summary.plus_2.dprime, summary.plus_3.dprime, summary.minus_4.dprime];
se_dprimeVec = [summary.minus_4.se_dprime, summary.minus_3.se_dprime, summary.minus_2.se_dprime, ...
    summary.minus_1.se_dprime, summary.no_ms_0.se_dprime, summary.plus_1.se_dprime, ...
    summary.plus_2.se_dprime, summary.plus_3.se_dprime, summary.minus_4.se_dprime];

noMsDprimeVec = [summary.no_ms_0.dprime, summary.no_ms_1.dprime, summary.no_ms_2.dprime, ...
    summary.no_ms_3.dprime, summary.no_ms_4.dprime];
se_noMsDprimeVec = [summary.no_ms_0.se_dprime, summary.no_ms_1.se_dprime, summary.no_ms_2.se_dprime, ...
    summary.no_ms_3.se_dprime, summary.no_ms_4.se_dprime];

figure('position', [700 350 1200 500])
errorbar(distances, dprimeVec, se_dprimeVec, 'ok', 'LineStyle', 'none', 'LineWidth', 2, ...
'MarkerFaceColor','w','Markersize',8)
hold on
plot([xLim 45], [summary.neutral.dprime, summary.neutral.dprime] , 'color', rgb('DarkRed'), 'LineWidth',2)
plot([xLim 45], [summary.neutral.dprime + summary.neutral.se_dprime,  summary.neutral.dprime + summary.neutral.se_dprime] , ...
'color', rgb('DarkRed'), 'LineStyle', '--', 'LineWidth', 2)
plot([xLim 45], [summary.neutral.dprime - summary.neutral.se_dprime, summary.neutral.dprime - summary.neutral.se_dprime] , ...
'color', rgb('DarkRed'), 'LineStyle', '--', 'LineWidth', 2)
set(gca, 'FontSize', 15, 'YTick', 0:.5:2)
xticks(distances)
ylabel("Sensitivity [d']")
xlabel('Distance from saccade target location [arcmin]')
ylim([-0.25 2])
xlim([xLim 45])
box off

% Add line of best fit
p = polyfit(distances, dprimeVec, 8);
y = polyval(p, distances);
plot(distances, y, 'color', rgb('DarkGreen'), 'LineWidth', 2)
loDprime = dprimeVec - se_dprimeVec; hiDprime = dprimeVec + se_dprimeVec;
hf1 = nan(length(loDprime)-1, 1);
for jj = 1:(length(loDprime)-1)
    hf1(jj) = fill([distances(jj:jj+1) distances(jj+1:-1:jj)], [loDprime(jj:jj+1) hiDprime(jj+1:-1:jj)], 'g');
end
hflFaceColor = rgb('DarkGreen');
set(hf1, 'FaceAlpha', .3, 'EdgeColor', 'none');
set(hf1, 'FaceColor', hflFaceColor);

print('-djpeg','./Documents/VSS/Presentation/MsNoMS.jpeg');
print('-depsc','./Documents/VSS/Presentation/MsNoMs.epsc');

%% Valid vs Invalid Neutral Condition
figure('Position', [900 350 700 500])
errorbar([summary.cong_neutral.dprime, summary.incong_neutral.dprime], [summary.cong_neutral.se_dprime, summary.incong_neutral.se_dprime], ...
'ok', 'LineStyle', 'none', 'LineWidth', 2, ...
'MarkerFaceColor','w','Markersize',8)
set(gca, 'XTick', 1:2, 'XTickLabel', {'Cong Neutral','Incong Neutral'}, 'FontSize', 15)
ylabel("Sensitivity [d']")
% ylim([floor(min([summary.cong_neutral.dprime, summary.incong_neutral.dprime])), ...
% ceil(max([summary.cong_neutral.dprime + summary.cong_neutral.se_dprime, summary.incong_neutral.dprime + summary.incong_neutral.se_dprime]))])
ylim([0 2])
xlim([.5 2.5])
box off
[~, p] = ttest(indv.cong_neutral.dprime, indv.incong_neutral.dprime)
print('-djpeg','./Documents/VSS/Presentation/FixationDprime.jpeg');
print('-depsc','./Documents/VSS/Presentation/FixationDprime.epsc');

%% Reaction Time

xLim = -45;
distances = [-41, -38, -29, -16, 0, 16, 29, 38, 41];
respTimeVec = [summary.minus_4.resp_time, summary.minus_3.resp_time, summary.minus_2.resp_time, ...
    summary.minus_1.resp_time, summary.minus_0.resp_time, summary.plus_1.resp_time, ...
    summary.plus_2.resp_time, summary.plus_3.resp_time, summary.minus_4.resp_time];
se_respTimeVec = [summary.minus_4.se_resp_time, summary.minus_3.se_resp_time, summary.minus_2.se_resp_time, ...
    summary.minus_1.se_resp_time, summary.minus_0.se_resp_time, summary.plus_1.se_resp_time, ...
    summary.plus_2.se_resp_time, summary.plus_3.se_resp_time, summary.minus_4.se_resp_time];
figure('position', [700 350 1200 500])
errorbar(distances, respTimeVec, se_respTimeVec, 'ok', 'LineStyle', 'none', 'LineWidth', 2, ...
'MarkerFaceColor','w','Markersize',8)
hold on
plot([xLim 45], [summary.neutral.resp_time, summary.neutral.resp_time] , 'color', rgb('DarkRed'), 'LineWidth',2)
plot([xLim 45], [summary.neutral.resp_time + summary.neutral.se_resp_time,  summary.neutral.resp_time + summary.neutral.se_resp_time] , ...
'color', rgb('DarkRed'), 'LineStyle', '--', 'LineWidth', 2)
plot([xLim 45], [summary.neutral.resp_time - summary.neutral.se_resp_time, summary.neutral.resp_time - summary.neutral.se_resp_time] , ...
'color', rgb('DarkRed'), 'LineStyle', '--', 'LineWidth', 2)
set(gca, 'FontSize', 15, 'YTick',400:100:1000)
xticks(distances)
ylabel("Response Time [ms]")
xlabel('Distance from saccade target location [arcmin]')
ylim([400 900])
xlim([xLim 45])
box off

plot(distances, respTimeVec, 'b-', 'LineWidth', 2)
loRespTime = respTimeVec - se_respTimeVec; hiRespTime = respTimeVec + se_respTimeVec;
hf1 = nan(length(loRespTime)-1, 1);
for jj = 1:(length(loDprime)-1)
    hf1(jj) = fill([distances(jj:jj+1) distances(jj+1:-1:jj)], [loRespTime(jj:jj+1) hiRespTime(jj+1:-1:jj)], 'b-');
end
hflFaceColor = rgb('DarkTurquoise');
set(hf1, 'FaceColor', hflFaceColor);
print('-djpeg','./Documents/VSS/Presentation/RespTime.jpeg');
print('-depsc','./Documents/VSS/Presentation/RespTime.epsc');


respTimeAnova = [indv.minus_4.resp_time; indv.minus_3.resp_time; indv.minus_2.resp_time; ...
    indv.minus_1.resp_time; indv.minus_0.resp_time; indv.plus_1.resp_time; ...
    indv.plus_2.resp_time; indv.plus_3.resp_time; indv.minus_4.resp_time]';
