function dataStruct = discardedTrialsFixation()

dataStruct.blinks = 0;
dataStruct.no_track = 0;
dataStruct.no_response = 0;
dataStruct.short_trial = 0;
dataStruct.saccades = 0;
dataStruct.microsaccade = 0;
dataStruct.gaze_off_center = 0;
dataStruct.manual_discard = 0;

end